﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SampleOrdersTrackingSystem.Models
{
    public class SalesOrderRequestProduct 
        //: INotifyPropertyChanged
    {

        //  [DataType(DataType.Currency)]

      
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public Guid SalesorderRequestId { get; set; }
        public string SKU { get; set; }
        public string Bin { get; set; }
        public string Description { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public Guid PackageId { get; set; }
        public string PackageName { get; set; }
       // private decimal mQty;
        [Required]
        [DataType("Integer")]
        [Range(0, int.MaxValue)]
        public decimal Quantity { get; set; }
        //public decimal Quantity
        //{
        //    get
        //    {
        //        return this.mQty;
        //    }
        //    set
        //    {
        //        SetProperty(ref this.mQty, value, "Quantity");
        //        TOTAL = Quantity * Cost;
        //    }
        //}




        //public decimal Quantity { get
        //    {
        //        return mQty;
        //    } set {
        //        PropertyChanged()
        //        TOTAL = Quantity * Cost;

        //} }
        public decimal Cost { get; set; }
        public decimal ActCost { get; set; }
        private decimal total;
        public decimal TOTAL { get; set; }
        //public decimal TOTAL { get {
        //        return this.total;
        //    } set {
        //        SetProperty(ref this.total, value, "Total");
        //    } }


        [Required]
        [Range(0, int.MaxValue)]
        public decimal Rebate { get; set; }
        public decimal DelivPrice { get; set; }
        public decimal ApprovedQty { get; set; }
        public decimal AprovedCost { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public int EventID { get; set; }
        public string EventName { get; set; }
        public string Remark { get; set; }
        public DateTime Inserted { get; set; }
        public string InsertAgent { get; set; }

        public decimal AvaliableQty { get; set; }

      


        public event PropertyChangedEventHandler PropertyChanged;

        //protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        //{
        //    var changed = PropertyChanged;
        //    if (changed == null)
        //        return;

        //    changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}

        //protected bool SetProperty<T>(ref T backingStore, T value, [CallerMemberName] string propertyName = "", Action onChanged = null)
        //{
        //    if (EqualityComparer<T>.Default.Equals(backingStore, value))
        //        return false;

        //    backingStore = value;
        //    onChanged?.Invoke();
        //    OnPropertyChanged(propertyName);
        //    return true;
        //}
    }
}
