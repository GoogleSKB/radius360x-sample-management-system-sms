﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class SalesOrderRequestCustomer
    {
		public Guid Id { get; set; }
		public Guid ClientId { get; set; }
		public Guid SalesorderRequestId { get; set; }
		public Guid CustomerId { get; set; }
		public string CustomerName { get; set; }
		public Guid BillAddressId { get; set; }
		public string BillAddress { get; set; }
		public Guid ShipAddressId { get; set; }
		public string ShipAddress { get; set; }
		public DateTime Inserted { get; set; }
		public string InsertAgent { get; set; }
	}
}
