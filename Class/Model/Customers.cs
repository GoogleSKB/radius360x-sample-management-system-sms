﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class Customers
    {
        public int Sampleid { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }
        public Guid ShipToAddressId { get; set; }
        public string ShipToAddressee { get; set; }
        public string Identifier { get; set; }
        public Guid TerritoryId { get; set; }
    }
}
