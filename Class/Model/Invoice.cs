﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class Invoice
    {
        public Guid ID { get; set; }  
        public string Createdby { get; set; }
        public string IsInserted { get; set; }
    }
}
