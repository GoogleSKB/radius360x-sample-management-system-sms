﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class SampleOrders
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public string RequestNo { get; set; }
        public Guid SalesRepID { get; set; }
        public string SalesRepName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime TxnDate { get; set; }
        public int Reasonforsample { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }
        public int SalesorderTypeID { get; set; }
        public DateTime Inserted { get; set; }
        public string InsertAgent { get; set; }
        public Guid DriverId { get; set; }
        public  string DriverName { get; set; }

       

        public IEnumerable<SalesOrderRequestProduct> SalesOrderRequestProducts { get; set; }
        public IEnumerable<SalesOrderRequestCustomer> SalesOrderRequestCustomers { get; set; }
        public IEnumerable<SalesOrderRequestProduct> DeleteProducts { get; set; }
        public IEnumerable<SalesOrderRequestCustomer> DeleteCustomers { get; set; }


    }
}
