﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class PurchaseOrders
    {
        public Guid Id { get; set; }
        public Guid VendorId { get; set; }
        public string VendorName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }
        public Guid TermsId { get; set; }
        public Guid ShipMethodId { get; set; }
        public string Shipvia { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DueDate { get; set; }
        public string ShipToEntityId { get; set; }
        public string InsertAgent { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ExpectedDate { get; set; }
        public IEnumerable<PurchaseOrderLineItems> PurchaseOrderLineItems { get; set; }


        // public string VendorIdentifier { get; set; }
        //public string VendorMessage { get; set; }
        //public string ReferenceNumber { get; set; }
        //public int TxnNumber { get; set; }
        //public string ShipMethodIdentifier { get; set; }
        //public string Memo { get; set; }
        //public string ClassIdentifier { get; set; }
        //public Guid ClassId { get; set; }
        //public string TermsIdentifier { get; set; }
        //
        //public decimal TotalAmount { get; set; }
        //public string TemplateIdentifier { get; set; }
        //public string CurrencyIdentifier { get; set; }
        //public Guid CurrencyId { get; set; }
        //public int ItemCount { get; set; }
        //public bool IsFullyReceived { get; set; }
        //public string IsManuallyClosed { get; set; }
        //public bool IsToBePrinted { get; set; }
        //public bool IsToBeEmailed { get; set; }
        //public bool IsTaxIncluded { get; set; }
        //public string SalesTaxCodeIdentifier { get; set; }
        //public Guid SalesTaxCodeId { get; set; }
        //public string FOB { get; set; }
        //public string VendorNote { get; set; }
        //public string ShipToEntityName { get; set; }
        //public string ShippingNote { get; set; }
        //public decimal ExchangeRate { get; set; }
        //public string Other1 { get; set; }
        //public string Other2 { get; set; }
        //public string CustomFields { get; set; }
        //public string EditSequence { get; set; }
        //public string APAccountRefIdentifier { get; set; }
        //public string TaxCodeRefIdentifier { get; set; }



    }
}
