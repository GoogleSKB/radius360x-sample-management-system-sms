﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class PurchaseOrderLineItems
    {
        
        public Guid Id { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public Guid ProductId { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string InsertAgent { get; set; }
        
        //public string Identifier { get; set; }
        //public string PurchaseOrderIdentifier { get; set; }
        //public DateTime Date { get; set; }
        //public string ProductIdentifier { get; set; }
        //public Guid ProductId { get; set; }
        //public string ProductName { get; set; }
        //public Guid ShipMethodId { get; set; }
        //public string Shipvia { get; set; }
        //public Guid TermsId { get; set; }
        //public string CustomerTerms { get; set; }
        //public string ItemGroupIdentifier { get; set; }
        //public Guid ItemGroupId { get; set; }
        //public string CustomerIdentifier { get; set; }
        //public string ItemUnitOfMeasure { get; set; }
        //public decimal LineAmount { get; set; }
        //public decimal ReceivedQuantity { get; set; }
        //public string ClassIdentifier { get; set; }
        //public string ClassId { get; set; }
        //public string ItemIsManuallyClosed { get; set; }
        //public string ItemPartNumber { get; set; }
        //public string ItemOther1 { get; set; }
        //public string ItemOther2 { get; set; }
        //public string CustomFields { get; set; }
        //public bool IsFullyReceived { get; set; }
        //public string IsManuallyClosed { get; set; }
        //public bool IsToBePrinted { get; set; }
        //public bool IsToBeEmailed { get; set; }
        //public bool IsTaxIncluded { get; set; }
        //public string SalesTaxCodeIdentifier { get; set; }
        //public Guid SalesTaxCodeId { get; set; }
        //public string FOB { get; set; }
        //public string VendorNote { get; set; }
        //public string ShipToEntityId { get; set; }
        //public string ShipToEntityName { get; set; }
        //public decimal ExchangeRate { get; set; }
        //public string Other1 { get; set; }
        //public string Other2 { get; set; }
        //public string EditSequence { get; set; }
        //public string LineNumber { get; set; }
        //public string ItemInventorySiteLocationIdentifier { get; set; }
        //public string ItemInventorySiteLocationName { get; set; }
        //public DateTime Inserted { get; set; }
        //public int InsertSource { get; set; }

    }
}
