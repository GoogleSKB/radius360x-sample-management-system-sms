﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class Budget
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public Guid SalesRepID { get; set; }
        public string SalesRepName { get; set; }
        public string SalesRepIdentifier { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime FromPeriod { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ToPeriod { get; set; }
        public int PeriodType { get; set; }
        public string PeriodTypeName { get; set; }
        public decimal BudAmount { get; set; }
        public bool Recurring { get; set; }
        public bool Active { get; set; }

        public DateTime Inserted { get; set; }
        public int InsertSource { get; set; }
        public string InsertAgent { get; set; }
        public DateTime Updated { get; set; }
        public int UpdateSource { get; set; }
        public string UpdateAgent { get; set; }

        public string PeriodRange { get; set; }

        public string SalesRepIDs { get; set; }
        public decimal BalAmount { get; set; }

        public IEnumerable<BudgetLineProducts> BudgetLineProducts { get; set; }


        // Logic for Update or Delete Budgetline products in Jquery Ajax call
        // public IEnumerable<BudgetLineProducts> NewBudgetLine { get; set; }
        // public IEnumerable<BudgetLineProducts> UpdatedBudgetLine { get; set; }
        // public IEnumerable<BudgetLineProducts> DeletedBudgetLine { get; set; }
    }
    public class BudgetLineProducts
    {
        public Nullable<Guid> Id { get; set; }
        public Nullable<Guid> ClientId { get; set; }
        public Nullable<Guid> BudgetId { get; set; }
        public string Description { get; set; }
        public string Bin { get; set; }
        public string  SKU { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }

        
        public Guid PackageId { get; set; }
        public string PackageName { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        [DisplayFormat(DataFormatString = "{0:0.##}", ApplyFormatInEditMode = true)]
        [DataType("Integer")]
        [Range(0, int.MaxValue)]
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal TOTAL { get; set; }
        
        [Range(0, int.MaxValue)]
        public decimal Rebate { get; set; }
        public decimal DelivPrice { get; set; }
        public decimal TotalAmount { get { return Cost * Quantity; } set { } }
        public bool Active { get; set; }
        public string BudgetIdentifier { get; set; }
        public DateTime Inserted { get; set; }
        public int InsertSource { get; set; }
        public string InsertAgent { get; set; }
        public decimal ActCost { get; set; }

        public decimal AvaliableQty { get; set; }



    }
}
