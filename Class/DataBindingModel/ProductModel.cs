﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
 
{
    public class ProductModel
    {
        public Nullable<Guid> Id { get; set; }
        public Nullable<Guid> ClientId { get; set; }
        public string Description { get; set; }
        public string SKU { get; set; }
        public  string Bin { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public Guid PackageId { get; set; }
        public string PackageName { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        [DisplayFormat(DataFormatString = "{0:0.##}", ApplyFormatInEditMode = true)]
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal TOTAL { get; set; }
        public decimal Rebate { get; set; }
        public decimal DelivPrice { get; set; }
        public decimal AvaliableQty { get; set; }
        public decimal TotalAmount { get { return Cost * Quantity; } set { } }
        public string Vendorname { get; set; }
        public string FullName { get; set; }

    }
}
