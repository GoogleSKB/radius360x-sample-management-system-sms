﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOTSLibrary.DataBindingModel
{
  public  class ShipviaCombo
    {
        public Guid ShipMethodId { get; set; }
        public string Shipvia { get; set; }
    }
}
