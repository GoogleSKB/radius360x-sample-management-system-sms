﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class PackageCombo
    {
        public Guid PackageId { get; set; }
        public string PackageName { get; set; }
    }
}
