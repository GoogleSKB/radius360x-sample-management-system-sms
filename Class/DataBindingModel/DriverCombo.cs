﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleOrdersTrackingSystem.Models
{
   public class DriverCombo
    {
        public Guid DriverId { get; set; }
        public string DriverName { get; set; }
    }
}
