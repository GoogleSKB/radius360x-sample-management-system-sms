﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Models
{
    public class ProductCombo
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string SKU { get; set; }
    }
}
