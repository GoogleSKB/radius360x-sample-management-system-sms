﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOTSLibrary.DataBindingModel
{
   public class VendorCombo
    {
        public Guid VendorId { get; set; }
        public string VendorName { get; set; }

    }
}
