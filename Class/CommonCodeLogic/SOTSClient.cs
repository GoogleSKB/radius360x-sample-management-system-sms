﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using System.Linq;
using SampleOrdersTrackingSystem.Models.Dropdown;
using SampleOrdersTrackingSystem.Models;
using SOTSLibrary.DataBindingModel;
using System.Threading.Tasks;
using System.Reflection;

namespace SOTSLibrary
{
    public class SOTSClient
    {

        #region "Connection"
        private string ConnectionString { get; set; }
             
        public SOTSClient(string ConStr)
        {
            ConnectionString = ConStr;
        }
        #endregion

        #region "Dispose"

        public void Dispose() 
        {
            Dispose(true);  
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
            }
            // free native resources if there are any.
        }
        #endregion

        #region SECTION SELECT QUERIES

        public IEnumerable<T> ListReader<T>(string SQL , string DbName)
        {
            using (IDbConnection db = new SqlConnection(DbName))
            {
                return db.Query<T>(SQL).ToList();
            }
        }

        public T FirstOrDefault<T>(string SQL, string DbName)
        {
            using (IDbConnection db = new SqlConnection(DbName))
            {
                  return db.Query<T>(SQL).First();
            }
        }
        //public static bool DynamicConnection(string Role)
        //{
        //    if (string.IsNullOrWhiteSpace(Role))
        //        return false;
        //    return char.IsUpper(ch);
        //}
        public string Select_Query_ShipMethods()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id as ShipMethodId");
            sb.AppendLine(" ,Name as Shipvia");
            sb.AppendLine(" FROM [dbo].[ShipMethods]");
            sb.AppendLine(" WHERE");
            sb.AppendLine(" Active  = 1 ");
            sb.AppendLine(" AND");
            sb.AppendLine(" IsDeleted = 0 ");
            sb.AppendLine(" ORDER BY Name");
            return sb.ToString();
        }

        public string Select_Query_CustomerTerms()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id");
            sb.AppendLine(" ,Name");
            sb.AppendLine(" FROM [dbo].[CustomerTerms]");
            sb.AppendLine(" WHERE");
            sb.AppendLine(" Active  = 1 ");
            sb.AppendLine(" AND");
            sb.AppendLine(" IsDeleted = 0 ");
            sb.AppendLine(" ORDER BY Name");
            return sb.ToString();
        }

        public string Select_Query_Customer()
        {
            StringBuilder sb = new StringBuilder("select c.id as CustomerId");
            sb.AppendLine(" , c.name as CustomerName");
            sb.AppendLine(" , c.ShipToAddressId");
            sb.AppendLine(" ,  (ISNULL(ADDBL.Extension1, '') + ' ' + ISNULL(ADDBL.extension2, '') + ' ' + ");
            sb.AppendLine(" ISNULL(ADDBL.City, '') + ' ' + ISNULL(ADDBL.State, '') + ' ' + ISNULL(ADDBL.PostalCode, '')) ShipToAddressee ");
            sb.AppendLine(" from  customers c inner join Addresses addbl on addbl.id = c.BillToAddressId");
            sb.AppendLine(" where c.IsDeleted  = 0  and c.Active = 1");

            return sb.ToString();
        }

        public string Select_Query_Product()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("  p.Name as Description");
            sb.AppendLine("  ,p.ManufacturerPartNumber as Bin");
            sb.AppendLine("  ,p.Id AS ProductID");
            sb.AppendLine(" , p.[SKU], p.FullName ");
            sb.AppendLine(" , p.[Description] as ProductName ");
            sb.AppendLine(" , InventoryPackageId AS PackageId");
            sb.AppendLine(" , pa.Name AS PackageName ");
            sb.AppendLine(" , 0 AS 'Quantity'");
            sb.AppendLine(" , 0 AS 'Rebate'");
            sb.AppendLine(" , PurchaseCost AS Cost");
            sb.AppendLine(" , SalesPrice AS DelivPrice ");
            sb.AppendLine(" , convert(decimal(18,2),ai.QtyOnHand-ai.QtyOnSalesOrder)  as AvaliableQty ");
            sb.AppendLine(" , v.Name AS Vendorname ");
            sb.AppendLine(" FROM Products p ");
            sb.AppendLine(" JOIN Packages pa ");
            sb.AppendLine(" ON pa.Id = p.InventoryPackageId");
            sb.AppendLine(" inner join AccountInventory ai on p.id =  ai.Productid");
            sb.AppendLine(" FULL OUTER JOIN Vendors v on p.PrefVendorRefListID =  v.Identifier");
            sb.AppendLine(" where p.IsDeleted  = 0  and p.Active = 1");
            return sb.ToString(); ;
        }

        public string Select_Query_Product_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("ID as ProductId,");
            sb.AppendLine("Name as ProductName,");
            sb.AppendLine("SKU ");
            sb.AppendLine("from  Products ");
            sb.AppendLine("where IsDeleted  = 0  and Active = 1 order by Name");
            return sb.ToString(); 
        }
 

        public string Select_Query_SalesRep_Combo(string salesrepname = null)
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("Id ,");
            sb.AppendLine("Name ");
            sb.AppendLine("from  Territories ");
            sb.AppendLine("where Active = 1 and IsDeleted = 0 ");
            if(salesrepname != null)
            {
              sb.AppendLine(" and Name ='" + salesrepname + "'");
            }

            sb.AppendLine("order by Name");
            return sb.ToString();
        }

        public string Select_Query_Customer_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("ID as CustomerId ,");
            sb.AppendLine("Name as CustomerName ");
            sb.AppendLine("from  customers ");
            sb.AppendLine("where IsDeleted  = 0  and Active = 1 ");
            sb.AppendLine("order by Name");
            return sb.ToString();
        }

        public string Select_Query_Driver_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("ID as DriverId ,");
            sb.AppendLine("Name as DriverName ");
            sb.AppendLine("from  Drivers ");
            sb.AppendLine("where IsDeleted  = 0  and Active = 1 ");
            sb.AppendLine("order by Name");
            return sb.ToString();
        }


        public string GetPurchaseOrders(Guid vendorId)
        {
            #region SELECT QUERY
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Select");
            stringBuilder.AppendLine("  po.[Id]");
            stringBuilder.AppendLine(",p.[SKU]");
            stringBuilder.AppendLine(",p.[Description]");
            stringBuilder.AppendLine(",polt.[Price]");
            stringBuilder.AppendLine(",polt.[Quantity]");
            stringBuilder.AppendLine(",c.[Name] as Customer");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[VendorIdentifier]");
            stringBuilder.AppendLine(",po.[VendorId]");
            stringBuilder.AppendLine(",po.[VendorMessage]");
            stringBuilder.AppendLine(",v.[Name] as VendorName");
            stringBuilder.AppendLine(",po.[ReferenceNumber]");
            stringBuilder.AppendLine(",po.[TxnNumber]");
            stringBuilder.AppendLine(",po.[Date]");
            stringBuilder.AppendLine(",po.[DueDate]");
            stringBuilder.AppendLine(",po.[ShipMethodIdentifier]");
            stringBuilder.AppendLine(",po.[ShipMethodId]");
            stringBuilder.AppendLine(",shm.[Name] as Shipvia");
            stringBuilder.AppendLine(",po.[ExpectedDate]");
            stringBuilder.AppendLine(",po.[Memo]");
            stringBuilder.AppendLine(",po.[ClassIdentifier]");
            stringBuilder.AppendLine(",po.[ClassId]");
            stringBuilder.AppendLine(",po.[TermsId]");
            stringBuilder.AppendLine(",po.[TermsIdentifier]");
            stringBuilder.AppendLine(",ct.Name as CustomerTerms");
            stringBuilder.AppendLine(",po.[TotalAmount]");
            stringBuilder.AppendLine(",po.[TemplateIdentifier]");
            stringBuilder.AppendLine(",po.[CurrencyIdentifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[CurrencyId]");
            stringBuilder.AppendLine(",po.[ItemCount]");
            stringBuilder.AppendLine(",po.[IsFullyReceived]");
            stringBuilder.AppendLine(",po.[IsManuallyClosed]");
            stringBuilder.AppendLine(",po.[IsToBePrinted]");
            stringBuilder.AppendLine(",po.[IsToBeEmailed]");
            stringBuilder.AppendLine(",po.[IsTaxIncluded]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeIdentifier]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeId]");
            stringBuilder.AppendLine(",po.[FOB]");
            stringBuilder.AppendLine(",po.[VendorNote]");
            stringBuilder.AppendLine(",po.[ShipToEntityName]");
            stringBuilder.AppendLine(",po.[ShipToEntityId]");
            stringBuilder.AppendLine(",po.[ShippingNote]");
            stringBuilder.AppendLine(",po.[ExchangeRate]");
            stringBuilder.AppendLine(",po.[Other1]");
            stringBuilder.AppendLine(",po.[Other2]");
            stringBuilder.AppendLine(",po.[CustomFields]");
            stringBuilder.AppendLine(",po.[EditSequence]");
            stringBuilder.AppendLine(",po.[APAccountRefIdentifier]");
            stringBuilder.AppendLine(",po.[TaxCodeRefIdentifier]");
            stringBuilder.AppendLine(",po.[Active]");
            stringBuilder.AppendLine(",po.[IsDeleted]");
            stringBuilder.AppendLine(",po.[Inserted]");
            stringBuilder.AppendLine(",po.[InsertSource]");
            stringBuilder.AppendLine(",po.[InsertAgent]");
            stringBuilder.AppendLine(",po.[Updated]");
            stringBuilder.AppendLine(",po.[UpdateSource]");
            stringBuilder.AppendLine(" , po.[UpdateAgent]");
            stringBuilder.AppendLine("FROM  [dbo].[PurchaseOrders] po");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[PurchaseOrderLineItems] polt");
            stringBuilder.AppendLine("  ON po.Id=polt.PurchaseOrderId");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[Vendors] v");
            stringBuilder.AppendLine("ON po.[VendorId]=v.[Id]");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[ShipMethods] shm");
            stringBuilder.AppendLine("ON po.[ShipMethodId]=shm.Id");
            stringBuilder.AppendLine("LEFT JOIN[CustomerTerms] ct");
            stringBuilder.AppendLine("ON po.TermsId= ct.Id");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[Products] p");
            stringBuilder.AppendLine("ON polt.ProductId = p.Id");
            stringBuilder.AppendLine("LEFT JOIN [dbo].[Customers] c");
            stringBuilder.AppendLine("ON polt.CustomerId = c.Id");
            stringBuilder.AppendLine("where IsDeleted  = 0  and Active = 1 ");
            //Guid.Empty
            if (vendorId != null || vendorId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                stringBuilder.AppendLine("Where v.Id='" + vendorId + "'");
            }
            return stringBuilder.ToString();
            #endregion         
        }

        #endregion

        #region SECTION COMMON CODE LOGIC
        public List<CommonDropdown> Get_Data_By_Query(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<CommonDropdown>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public List<ShipviaCombo> Get_Data_By_Shipvia_Combo(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<ShipviaCombo>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public List<SalesRapCombo> Get_Data_By_Salesrep_Combo(string query)
        {

            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<SalesRapCombo>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Customers> Get_Data_By_Customer_Combo(string query)
        {

            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<Customers>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<ProductCombo> Get_Data_By_Product_Combo(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<ProductCombo>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<ProductModel> Get_ProductLineItemsModal(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<ProductModel>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Customers> Get_CustomerModal(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<Customers>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<VendorCombo> Get_Data_Vendor(string query)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<VendorCombo>
                    (query).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<BudgetLineProducts> Get_List_Query_Product_Modal_Budget()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("  p.Id AS ProductID");
            sb.AppendLine(" , p.[Description] as ProductName ");
            sb.AppendLine(" , InventoryPackageId AS PackageId");
            sb.AppendLine(" , pa.Name AS PackageName ");
            sb.AppendLine(" , 0.0 AS 'Quantity'");
            sb.AppendLine(" , 0.0 AS 'Rebate'");
            sb.AppendLine(" , SalesPrice AS Cost");
            sb.AppendLine(" , 0.0 AS 'DelivPrice' ");
            sb.AppendLine(" FROM Products p ");
            sb.AppendLine(" JOIN Packages pa ");
            sb.AppendLine(" ON pa.Id = p.InventoryPackageId");
            sb.AppendLine("where p.IsDeleted  = 0  and p.Active = 1 ");
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<BudgetLineProducts>
                 (sb.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<SalesRapCombo> Get_List_Query_SalesRep_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id");
            sb.AppendLine(" ,Name");
            sb.AppendLine(" FROM [dbo].[Territories]");
            sb.AppendLine(" WHERE");
            sb.AppendLine(" Active  = 1 ");
            sb.AppendLine(" AND");
            sb.AppendLine(" IsDeleted = 0 ");
            sb.AppendLine(" ORDER BY Name");
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<SalesRapCombo>
                 (sb.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public List<PackageCombo> Get_List_Query_Packages_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id as PackageId");
            sb.AppendLine(" ,Name as PackageName");
            sb.AppendLine(" FROM [dbo].[Packages]");
            sb.AppendLine(" WHERE Active  = 1 and  IsDeleted  = 0 ");
            sb.AppendLine(" ORDER BY Name");
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                  return db.Query<PackageCombo>
                  (sb.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Budget> Get_List_Query_Budget(string username = null)
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine("   b.Id");
            sb.AppendLine(" , b.SalesRepID");
            sb.AppendLine(" , t.Name as SalesRepName");
            sb.AppendLine(" , b.BudAmount");
            sb.AppendLine(" , b.fromPeriod,  b.ToPeriod");
            sb.AppendLine(" , b.Recurring");
            sb.AppendLine(" , b.Active , b.PeriodRange");
            sb.AppendLine(" , CASE");
            sb.AppendLine("  WHEN b.PeriodType = 1 THEN 'Weekly'");
            sb.AppendLine("  WHEN b.PeriodType = 2 THEN 'Monthly' ");
            sb.AppendLine("  WHEN b.PeriodType = 3 THEN 'Quarterly'");
            sb.AppendLine("  WHEN b.PeriodType = 4 THEN 'Half Yearly' ");
            sb.AppendLine("  WHEN b.PeriodType = 5 THEN 'Yearly' ");
            sb.AppendLine(" ELSE 'Custom Period' ");
            sb.AppendLine(" END AS PeriodTypeName ");
            sb.AppendLine(" FROM");
            sb.AppendLine(" [Sample].[Budget] b ");
            sb.AppendLine(" Inner Join Territories t ");
            sb.AppendLine(" on t.Id = b.SalesRepID ");
            sb.AppendLine(" Where b.Active =  1 and b.IsDeleted = 0");
            sb.AppendLine(" and CONVERT(date, GETDATE()) between b.FromPeriod  and b.ToPeriod");
            sb.AppendLine(" or b.FromPeriod > CONVERT(date, GETDATE())");
            if (username != null)
            {
                sb.AppendLine(" and t.Name ='" + username + "'");
            }
            //if(fromdate != null && Todate != null)
            //{
            //    sb.AppendLine(" and b.FromPeriod between '" + fromdate + "' and '" + Todate + "'");
            //}
            sb.AppendLine(" ORDER BY Name");

            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<Budget>
                 (sb.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<ProductCombo> Get_List_Query_Product_Combo()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id as ProductId");
            sb.AppendLine(" ,Name as ProductName");
            sb.AppendLine(" ,SKU");
            sb.AppendLine(" FROM [dbo].[Products]");
            sb.AppendLine(" WHERE");
            sb.AppendLine(" Active  = 1 ");
            sb.AppendLine(" AND");
            sb.AppendLine(" IsDeleted = 0 ");
            sb.AppendLine(" ORDER BY Name");
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<ProductCombo>
                 (sb.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        // Sample Get Methods 

        public IEnumerable<SampleOrders> GetSampleorderlist(string username = null, string status = null)
        {
            try
            {
                StringBuilder Sqlquery = new StringBuilder();
                Sqlquery.AppendLine("Select");
                Sqlquery.AppendLine(" s.Id");
                Sqlquery.AppendLine(" ,s.SalesRepID");
                Sqlquery.AppendLine(" ,t.name as SalesRepName");
                Sqlquery.AppendLine(" ,s.DriverId");
                Sqlquery.AppendLine(" ,d.name as DriverName");
                Sqlquery.AppendLine(" ,s.Status");
                Sqlquery.AppendLine(" ,s.TxnDate");
                Sqlquery.AppendLine(" ,s.RequestNo");
                Sqlquery.AppendLine(" from Sample.SalesOrderRequest s");
                Sqlquery.AppendLine(" inner join Territories t on t.id = s.SalesRepID");
                Sqlquery.AppendLine(" left  join Drivers d on d.id = s.DriverId");
                Sqlquery.AppendLine(" Where s.Active =  1 and s.IsDeleted = 0");
               // Sqlquery.AppendLine(" and s.TxnDate between CONVERT(date, DATEADD(WEEK,-4,GETUTCDATE())) AND  CONVERT(date, GETUTCDATE())");
                if (username != null)
                {
                    Sqlquery.AppendLine(" and t.Name ='" + username + "'");
                     
                }
                if (status != null)
                {
                    Sqlquery.AppendLine(" and s.Status ='" + status + "'");

                }
                Sqlquery.AppendLine(" Order by s.Inserted desc");
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<SampleOrders>
                    (Sqlquery.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string Select_Query_Vendor()
        {
            StringBuilder sb = new StringBuilder(" Select ");
            sb.AppendLine(" Id as VendorId  ");
            sb.AppendLine(" ,Name as VendorName");
            sb.AppendLine(" FROM [dbo].[Vendors]");
            sb.AppendLine(" WHERE");
            sb.AppendLine(" IsActive  = 1 ");
            sb.AppendLine(" AND");
            sb.AppendLine(" IsDeleted = 0 ");
            sb.AppendLine(" ORDER BY Name");
            return sb.ToString();
        }

        public List<PurchaseOrders> GetPurchaseOrdersList(Guid vendorId)
        {
            #region SELECT QUERY
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Select");
            stringBuilder.AppendLine("  po.[Id]");
            stringBuilder.AppendLine(",p.[SKU]");
            stringBuilder.AppendLine(",p.[Description]");
            stringBuilder.AppendLine(",polt.[Price]");
            stringBuilder.AppendLine(",polt.[Quantity]");
            stringBuilder.AppendLine(",c.[Name] as Customer");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[VendorIdentifier]");
            stringBuilder.AppendLine(",po.[VendorId]");
            stringBuilder.AppendLine(",po.[VendorMessage]");
            stringBuilder.AppendLine(",v.[Name] as VendorName");
            stringBuilder.AppendLine(",po.[ReferenceNumber]");
            stringBuilder.AppendLine(",po.[TxnNumber]");
            stringBuilder.AppendLine(",po.[Date]");
            stringBuilder.AppendLine(",po.[DueDate]");
            stringBuilder.AppendLine(",po.[ShipMethodIdentifier]");
            stringBuilder.AppendLine(",po.[ShipMethodId]");
            stringBuilder.AppendLine(",shm.[Name] as Shipvia");
            stringBuilder.AppendLine(",po.[ExpectedDate]");
            stringBuilder.AppendLine(",po.[Memo]");
            stringBuilder.AppendLine(",po.[ClassIdentifier]");
            stringBuilder.AppendLine(",po.[ClassId]");
            stringBuilder.AppendLine(",po.[TermsId]");
            stringBuilder.AppendLine(",po.[TermsIdentifier]");
            stringBuilder.AppendLine(",ct.Name as CustomerTerms");
            stringBuilder.AppendLine(",po.[TotalAmount]");
            stringBuilder.AppendLine(",po.[TemplateIdentifier]");
            stringBuilder.AppendLine(",po.[CurrencyIdentifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[CurrencyId]");
            stringBuilder.AppendLine(",po.[ItemCount]");
            stringBuilder.AppendLine(",po.[IsFullyReceived]");
            stringBuilder.AppendLine(",po.[IsManuallyClosed]");
            stringBuilder.AppendLine(",po.[IsToBePrinted]");
            stringBuilder.AppendLine(",po.[IsToBeEmailed]");
            stringBuilder.AppendLine(",po.[IsTaxIncluded]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeIdentifier]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeId]");
            stringBuilder.AppendLine(",po.[FOB]");
            stringBuilder.AppendLine(",po.[VendorNote]");
            stringBuilder.AppendLine(",po.[ShipToEntityName]");
            stringBuilder.AppendLine(",po.[ShipToEntityId]");
            stringBuilder.AppendLine(",po.[ShippingNote]");
            stringBuilder.AppendLine(",po.[ExchangeRate]");
            stringBuilder.AppendLine(",po.[Other1]");
            stringBuilder.AppendLine(",po.[Other2]");
            stringBuilder.AppendLine(",po.[CustomFields]");
            stringBuilder.AppendLine(",po.[EditSequence]");
            stringBuilder.AppendLine(",po.[APAccountRefIdentifier]");
            stringBuilder.AppendLine(",po.[TaxCodeRefIdentifier]");
            stringBuilder.AppendLine(",po.[Active]");
            stringBuilder.AppendLine(",po.[IsDeleted]");
            stringBuilder.AppendLine(",po.[Inserted]");
            stringBuilder.AppendLine(",po.[InsertSource]");
            stringBuilder.AppendLine(",po.[InsertAgent]");
            stringBuilder.AppendLine(",po.[Updated]");
            stringBuilder.AppendLine(",po.[UpdateSource]");
            stringBuilder.AppendLine(" , po.[UpdateAgent]");
            stringBuilder.AppendLine("FROM  [dbo].[PurchaseOrders] po");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[PurchaseOrderLineItems] polt");
            stringBuilder.AppendLine("  ON po.Id=polt.PurchaseOrderId");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[Vendors] v");
            stringBuilder.AppendLine("ON po.[VendorId]=v.[Id]");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[ShipMethods] shm");
            stringBuilder.AppendLine("ON po.[ShipMethodId]=shm.Id");
            stringBuilder.AppendLine("LEFT JOIN[CustomerTerms] ct");
            stringBuilder.AppendLine("ON po.TermsId= ct.Id");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[Products] p");
            stringBuilder.AppendLine("ON polt.ProductId = p.Id");
            stringBuilder.AppendLine("LEFT JOIN [dbo].[Customers] c");
            stringBuilder.AppendLine("ON polt.CustomerId = c.Id");
            //Guid.Empty
            if (vendorId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                stringBuilder.AppendLine("Where v.Id='" + vendorId + "'");
            }
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<PurchaseOrders>
                        (stringBuilder.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<PurchaseOrderLineItems> GetPurchaseOrderLineItemsList(string vendorId)
        {
            #region SELECT QUERY
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Select");
            stringBuilder.AppendLine("  po.[Id]");
            stringBuilder.AppendLine(",p.[SKU]");
            stringBuilder.AppendLine(",p.[Description]");
            stringBuilder.AppendLine(",polt.[Price]");
            stringBuilder.AppendLine(",polt.[Quantity]");
            stringBuilder.AppendLine(",c.[Name] as Customer");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[VendorIdentifier]");
            stringBuilder.AppendLine(",po.[VendorId]");
            stringBuilder.AppendLine(",po.[VendorMessage]");
            stringBuilder.AppendLine(",v.[Name] as VendorName");
            stringBuilder.AppendLine(",po.[ReferenceNumber]");
            stringBuilder.AppendLine(",po.[TxnNumber]");
            stringBuilder.AppendLine(",po.[Date]");
            stringBuilder.AppendLine(",po.[DueDate]");
            stringBuilder.AppendLine(",po.[ShipMethodIdentifier]");
            stringBuilder.AppendLine(",po.[ShipMethodId]");
            stringBuilder.AppendLine(",shm.[Name] as Shipvia");
            stringBuilder.AppendLine(",po.[ExpectedDate]");
            stringBuilder.AppendLine(",po.[Memo]");
            stringBuilder.AppendLine(",po.[ClassIdentifier]");
            stringBuilder.AppendLine(",po.[ClassId]");
            stringBuilder.AppendLine(",po.[TermsId]");
            stringBuilder.AppendLine(",po.[TermsIdentifier]");
            stringBuilder.AppendLine(",ct.Name as CustomerTerms");
            stringBuilder.AppendLine(",po.[TotalAmount]");
            stringBuilder.AppendLine(",po.[TemplateIdentifier]");
            stringBuilder.AppendLine(",po.[CurrencyIdentifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[Identifier]");
            stringBuilder.AppendLine(",po.[CurrencyId]");
            stringBuilder.AppendLine(",po.[ItemCount]");
            stringBuilder.AppendLine(",po.[IsFullyReceived]");
            stringBuilder.AppendLine(",po.[IsManuallyClosed]");
            stringBuilder.AppendLine(",po.[IsToBePrinted]");
            stringBuilder.AppendLine(",po.[IsToBeEmailed]");
            stringBuilder.AppendLine(",po.[IsTaxIncluded]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeIdentifier]");
            stringBuilder.AppendLine(",po.[SalesTaxCodeId]");
            stringBuilder.AppendLine(",po.[FOB]");
            stringBuilder.AppendLine(",po.[VendorNote]");
            stringBuilder.AppendLine(",po.[ShipToEntityName]");
            stringBuilder.AppendLine(",po.[ShipToEntityId]");
            stringBuilder.AppendLine(",po.[ShippingNote]");
            stringBuilder.AppendLine(",po.[ExchangeRate]");
            stringBuilder.AppendLine(",po.[Other1]");
            stringBuilder.AppendLine(",po.[Other2]");
            stringBuilder.AppendLine(",po.[CustomFields]");
            stringBuilder.AppendLine(",po.[EditSequence]");
            stringBuilder.AppendLine(",po.[APAccountRefIdentifier]");
            stringBuilder.AppendLine(",po.[TaxCodeRefIdentifier]");
            stringBuilder.AppendLine(",po.[Active]");
            stringBuilder.AppendLine(",po.[IsDeleted]");
            stringBuilder.AppendLine(",po.[Inserted]");
            stringBuilder.AppendLine(",po.[InsertSource]");
            stringBuilder.AppendLine(",po.[InsertAgent]");
            stringBuilder.AppendLine(",po.[Updated]");
            stringBuilder.AppendLine(",po.[UpdateSource]");
            stringBuilder.AppendLine(" , po.[UpdateAgent]");
            stringBuilder.AppendLine("FROM  [dbo].[PurchaseOrders] po");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[PurchaseOrderLineItems] polt");
            stringBuilder.AppendLine("  ON po.Id=polt.PurchaseOrderId");
            stringBuilder.AppendLine("  LEFT JOIN [dbo].[Vendors] v");
            stringBuilder.AppendLine("ON po.[VendorId]=v.[Id]");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[ShipMethods] shm");
            stringBuilder.AppendLine("ON po.[ShipMethodId]=shm.Id");
            stringBuilder.AppendLine("LEFT JOIN[CustomerTerms] ct");
            stringBuilder.AppendLine("ON po.TermsId= ct.Id");
            stringBuilder.AppendLine("LEFT JOIN[dbo].[Products] p");
            stringBuilder.AppendLine("ON polt.ProductId = p.Id");
            stringBuilder.AppendLine("LEFT JOIN [dbo].[Customers] c");
            stringBuilder.AppendLine("ON polt.CustomerId = c.Id");
            if (vendorId != "")
            {
                stringBuilder.AppendLine("Where v.Id='" + vendorId + "'");
            }
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    return db.Query<PurchaseOrderLineItems>
                        (stringBuilder.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        #endregion

    }
}
