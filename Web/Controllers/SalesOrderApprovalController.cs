﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleOrdersTrackingSystem.Models;
using System.Web.Mvc;
using BindAttribute = Microsoft.AspNetCore.Mvc.BindAttribute;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SampleOrdersTrackingSystem.Extensions;
using SelectSampleReasonList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using SelectBranchList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Transactions;
using CommandType = System.Data.CommandType;
using SOTSLibrary;
using SampleOrdersTrackingSystem.Models.Dropdown;
using SOTSLibrary.DataBindingModel;

namespace SampleOrdersTrackingSystem.Controllers
{
    public class SalesOrderApprovalController : Controller
    {
        #region SECTION GLOBAL VARS
        private readonly IWebHostEnvironment _webHostEnviroment;
        private readonly string _connectionString;
        public Guid objId;
        private ISession _session;
        SOTSClient sOTSClient;
        #endregion

        #region SECTION INITIALIZE CONNECTIONS

        public SalesOrderApprovalController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            // _connectionString = configuration.GetConnectionString("DefaultConnection");
            _session = httpContextAccessor.HttpContext.Session;
            _connectionString = httpContextAccessor.HttpContext.Session.GetString("DynamicConnection");
        }
        #endregion

        public IActionResult Index()
        {
            //ViewBag.formName = "";
            //ViewBag.ViewName = "";
            //ViewBag.ControllerName = "";
            string Uname = "";
            sOTSClient = new SOTSClient(_connectionString);
            IEnumerable<SampleOrders> lstsample = new List<SampleOrders>();
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                Uname = this.HttpContext.Session.GetString("Uname");
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(Uname, "1");
            }
            else if (HttpContext.Session.GetString("Role") == "ClientAdmin" || HttpContext.Session.GetString("Role") == "Client")
            {
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "1");
            }
            else
            {
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "0");
                ViewBag.vmessage = "You have No right for Approval Request";
            }
            //ViewBag.formName = "Sample Request Approval";
            //ViewBag.ViewName = "Index";
            //ViewBag.ControllerName = "Sales Order Approval";
            return View(lstsample);
        }

        public IActionResult ApprovedSampleRequest()
        {
            //ViewBag.formName = "";
            //ViewBag.ViewName = "";
            //ViewBag.ControllerName = "";
            string Uname = "";
            sOTSClient = new SOTSClient(_connectionString);
            IEnumerable<SampleOrders> lstsample = new List<SampleOrders>();
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                Uname = this.HttpContext.Session.GetString("Uname");
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(Uname, "2");
            }
            else if (HttpContext.Session.GetString("Role") == "ClientAdmin" || HttpContext.Session.GetString("Role") == "Client")
            {
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "2");
            }
            else
            {
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "0");
                ViewBag.vmessage = "You have No right for Approved Request";
            }
            //ViewBag.formName = "Approved Sample Request";
            //ViewBag.ViewName = "Approved";
            //ViewBag.ControllerName = "Sales Order Approval";
            return View(lstsample);
        }

        public IActionResult Approval(Guid pId)
        {
            if (pId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                ApprovalBySampleId(pId);
            }
            return RedirectToAction("Index", "SalesOrderApproval");
        }

        public IActionResult ApprovedView(Guid pId)
        {
            SampleOrders sample = new SampleOrders();

            string prop = "and bl.SalesorderRequestId = '" + pId + "'";
            string custp = "and sc.SalesorderRequestId = '" + pId + "'";

            sample = EditSampleById(pId);
            sample.SalesOrderRequestProducts = GetSampleProductlist(prop);
            sample.SalesOrderRequestCustomers = GetSampleCustomerlist(custp);
            return View(sample);
        }

        public IActionResult View(Guid pId)
        {
            SampleOrders sample = new SampleOrders();

            string prop = "and bl.SalesorderRequestId = '" + pId + "'";
            string custp = "and sc.SalesorderRequestId = '" + pId + "'";

            sample = EditSampleById(pId);
            ViewBag.sampleid = pId.ToString();
            sample.SalesOrderRequestProducts = GetSampleProductlist(prop);
            sample.SalesOrderRequestCustomers = GetSampleCustomerlist(custp);
            return View(sample);
        }
        public SampleOrders ApprovalBySampleId(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    string updateQuery = @"update Sample.SalesOrderRequest set Status = 2 where id = '" + id.ToString() + "' EXEC Sample.Generate_SalesOrderRequestToSO";
                    db.Execute(updateQuery);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public SampleOrders EditSampleById(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SampleOrders>("Select s.*, d.Name as DriverName , t.name as SalesRepName from Sample.SalesOrderRequest s " +
                        " inner join Territories t on t.id = s.SalesRepID " +
                        " left join Drivers d on d.id = s.DriverId " +
                        "where s.id = '" + id.ToString() + "'").First();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<SalesOrderRequestProduct> GetSampleProductlist(string productpara)
        {
            try
            {
                string sql = "Select bl.Id, p.Name as Description, p.ManufacturerPartNumber as Bin, p.SKU, bl.SalesorderRequestId, bl.ClientId, bl.SalesorderRequestId, bl.ProductID, p.name as ProductName , bl.Quantity, " +
                      " bl.Cost, bl.Rebate, bl.DelivPrice, bl.PackageId, pck.Name as PackageName " +
                      " ,bl.ApprovedQty, bl.AprovedCost, bl.StatusId, bl.TOTAL " +
                      " from Sample.SalesOrderRequestProducts bl " +
                      " inner join Products p on p.id = bl.ProductID" +
                      " inner join Packages pck on pck.id = bl.PackageId where bl.IsDeleted  = 0  and bl.Active = 1 " + productpara;

                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SalesOrderRequestProduct>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<SalesOrderRequestCustomer> GetSampleCustomerlist(string paraid)
        {
            try
            {
                string sql = "select sc.id,  sc.CustomerId, sc.ShipAddressId, c.name as Customername , c.ShipToAddressId ,  (ISNULL(ADDBL.Extension1, '') + ' ' + ISNULL(ADDBL.extension2, '') + ' ' +  " +
                             " ISNULL(ADDBL.City, '') + ' ' + ISNULL(ADDBL.State, '') + ' ' + ISNULL(ADDBL.PostalCode, '')) ShipAddress " +
                             " from  Sample.SalesOrderRequestCustomers sc " +
                             " inner join customers c  on c.Id = sc.CustomerId " +
                             " inner join Addresses addbl on addbl.id = c.BillToAddressId where sc.IsDeleted  = 0  and sc.Active = 1 " + paraid;

                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SalesOrderRequestCustomer>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IActionResult Approval_ProductRead([DataSourceRequest] DataSourceRequest request, Guid Id)
        {
            try
            {
                List<SalesOrderRequestProduct> productlist = new List<SalesOrderRequestProduct>();
                string productpara = "and bl.SalesorderRequestId = '" + Id + "'";
                productlist = GetSampleProductlist(productpara);
                DataSourceResult result = productlist.ToDataSourceResult(request, p => new Models.SalesOrderRequestProduct
                {
                    Id = p.Id,
                    SalesorderRequestId = p.SalesorderRequestId,
                    SKU = p.SKU,
                    Bin = p.Bin,
                    Description = p.Description,
                    ClientId = p.ClientId,
                    ProductID = p.ProductID,
                    ProductName = p.ProductName,
                    PackageId = p.PackageId,
                    PackageName = p.PackageName,
                    Cost = p.Cost,
                    Quantity = p.Quantity,
                    Rebate = p.Rebate,
                    DelivPrice = p.DelivPrice,
                    AvaliableQty = p.AvaliableQty,
                    TOTAL = p.TOTAL,
                    ApprovedQty = (p.ApprovedQty != 0) ? p.ApprovedQty : p.Quantity,
                    AprovedCost = (p.AprovedCost != 0) ? p.AprovedCost : p.Cost,
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<IActionResult> ApprovalProducts([FromBody] SampleOrders sample)
        { 
            try
            {
                  Guid objId;
                var sqlStatement = "";
                if (sample != null && ModelState.IsValid)
                {
                    objId = sample.Id; 
                    foreach (var samproduct in sample.SalesOrderRequestProducts)
                    {
                        if (samproduct != null && ModelState.IsValid)
                        {
                            samproduct.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            samproduct.TOTAL = ((samproduct.AprovedCost * ((100 - samproduct.Rebate) / 100)) * samproduct.ApprovedQty);
                            //samproduct.SalesorderRequestId = objId;

                            if (samproduct.SalesorderRequestId == null || samproduct.SalesorderRequestId == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                            {
                                //Guid obj = Guid.NewGuid();
                                samproduct.SalesorderRequestId = objId;
                                sqlStatement = @"INSERT INTO [Sample].[SalesOrderRequestProducts]  (  
                                                        Id , ClientId, SalesorderRequestId, ProductID,  
                                                        PackageId , Quantity, Cost, TOTAL, ApprovedQty , AprovedCost,  StatusId,   
                                                        Rebate, DelivPrice, Active, Inserted, InsertSource, 
                                                        InsertAgent,Updated, UpdateSource,
                                                        UpdateAgent, ProductIdentifier)  
                                            Values( NEWID(), 
                                                    (select ClientId  from Users where name = @InsertAgent),
                                                    @SalesorderRequestId, 
                                                    @ProductID, 
                                                    @PackageId, 
                                                    @Quantity, 
                                                    @Cost, 
                                                    @TOTAL,
                                                    @ApprovedQty, 
                                                    @AprovedCost, 
                                                    1, 
                                                    @Rebate, 
                                                    @DelivPrice,
                                                    1, 
                                                    GETUTCDATE(), 
                                                    1, 
                                                    @InsertAgent,  
                                                    GETUTCDATE(), 
                                                    1, 
                                                    @InsertAgent, 
                                                    (select Identifier from Products where id = @ProductID)
                                                )"; 
                            }
                            else
                            {
                                objId = sample.Id;
                                samproduct.SalesorderRequestId = objId;
                                sqlStatement = @"
                                                UPDATE Sample.SalesOrderRequestProducts  
                                                SET ApprovedQty = @ApprovedQty
                                                   ,AprovedCost = @AprovedCost
                                                   ,Rebate = @Rebate
                                                   ,TOTAL = @TOTAL 
                                                   ,StatusId = 2     
                                                WHERE Id = @Id and SalesorderRequestId = @SalesorderRequestId";
                            }
                            using (var connection = new SqlConnection(_connectionString))
                            {
                                await connection.OpenAsync();
                                await connection.ExecuteAsync(sqlStatement, samproduct);
                            }

                        }
                    }

                    foreach (var delsamproduct in sample.DeleteProducts)
                    {
                        if (delsamproduct != null && ModelState.IsValid)
                        {
                            delsamproduct.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            delsamproduct.SalesorderRequestId = objId;

                            using (var connection = new SqlConnection(_connectionString))
                            {
                                await connection.OpenAsync();
                                var sqlDel = "UPDATE Sample.SalesOrderRequestProducts " +
                                                    "SET Active = 0  " +
                                                    ", IsDeleted = 1 " +
                                                    ", StatusId  = 1 " +
                                                    "WHERE Id = @Id and SalesorderRequestId = @SalesorderRequestId";
                                await connection.ExecuteAsync(sqlDel, new { Id = delsamproduct.Id, SalesorderRequestId = delsamproduct.SalesorderRequestId });
                            }
                        }
                    }
                }
                return new Microsoft.AspNetCore.Mvc.JsonResult("1");
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }
         

    }
}
