﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleOrdersTrackingSystem.Models;
using System.Web.Mvc;
using BindAttribute = Microsoft.AspNetCore.Mvc.BindAttribute;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SampleOrdersTrackingSystem.Extensions;
using SelectPeriodtypeList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using SOTSLibrary;
using SOTSLibrary.DataBindingModel;


namespace SampleOrdersTrackingSystem.Controllers
{
    public class ModalFillController : Controller
    {


        #region SECTION GLOBAL VARS
        private readonly IWebHostEnvironment _webHostEnviroment;
        private readonly string _connectionString;
        public Guid objId;
        private ISession _session; 
        SOTSClient sOTSClient;
        #endregion

        #region SECTION INITIALIZE CONNECTIONS
        public ISession Session { get { return _session; } }

        public ModalFillController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
              System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            //_connectionString = configuration.GetConnectionString("DefaultConnection");
            _session = httpContextAccessor.HttpContext.Session;
            _connectionString = httpContextAccessor.HttpContext.Session.GetString("DynamicConnection");
        }

        #endregion

        public IActionResult ProductModal_Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                List<ProductModel> list = new List<ProductModel>();
                sOTSClient = new SOTSClient(_connectionString);
                var query = sOTSClient.Select_Query_Product();
                 list = sOTSClient.Get_ProductLineItemsModal(query);
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.ProductModel
                {
                    Id = p.Id,
                    Description = p.Description,
                    SKU = p.SKU,
                    Bin = p.Bin,
                    ProductID = p.ProductID,
                    PackageId= p.PackageId,
                    PackageName = p.PackageName,
                    ProductName = p.ProductName,
                    DelivPrice = p.DelivPrice,
                    Quantity = p.Quantity,
                    Cost = p.Cost,
                    Rebate = p.Rebate,
                    TOTAL = p.TOTAL,
                    AvaliableQty = p.AvaliableQty,
                    Vendorname = p.Vendorname,
                    FullName = p.FullName,
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public IActionResult CustomerModal_Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                List<Customers> list = new List<Customers>();
                sOTSClient = new SOTSClient(_connectionString);
                var query = sOTSClient.Select_Query_Customer();
                list = sOTSClient.Get_CustomerModal(query);
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.Customers
                {
                    CustomerId = p.CustomerId,
                    CustomerName = p.CustomerName,
                    ShipToAddressId = p.ShipToAddressId,
                    ShipToAddressee = p.ShipToAddressee
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public IActionResult AssignedCustomerModal_Read([DataSourceRequest] DataSourceRequest request, string salesrep)
        {
            try
            {
                List<Customers> list = new List<Customers>();
                sOTSClient = new SOTSClient(_connectionString);
                string Sql = "select sc.CustomerId, c.name as Customername , c.ShipToAddressId , " +
                              " (ISNULL(ADDBL.Extension1, '') + ' ' + ISNULL(ADDBL.extension2, '') + ' ' + " +
                              " ISNULL(ADDBL.City, '') + ' ' + ISNULL(ADDBL.State, '') + ' ' + ISNULL(ADDBL.PostalCode, '')) ShipToAddressee " +
                              " from   SalesCalls sc " +
                              " inner join customers c  on c.Id = sc.CustomerId " +
                              " inner join Addresses addbl on addbl.id = c.BillToAddressId " +
                              " where sc.IsDeleted = 0  and sc.Active = 1 and sc.TerritoryId in (select Id from Territories where name = '" + salesrep + "')";
                list = sOTSClient.Get_CustomerModal(Sql);
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.Customers
                {
                    CustomerId = p.CustomerId,
                    CustomerName = p.CustomerName,
                    ShipToAddressId = p.ShipToAddressId,
                    ShipToAddressee = p.ShipToAddressee,
                    TerritoryId = p.TerritoryId,
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
