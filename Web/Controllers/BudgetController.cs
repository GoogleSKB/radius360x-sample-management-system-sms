﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SampleOrdersTrackingSystem.Models;
using System.Web.Mvc;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SelectPeriodtypeList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using Ids = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using System.Transactions;
using CommandType = System.Data.CommandType;
using SOTSLibrary;
using JsonResult = System.Web.Mvc.JsonResult;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SampleOrdersTrackingSystem.Controllers
{
    public class BudgetController : Controller
    {
        #region SECTION GLOBAL VARS
        private readonly IWebHostEnvironment _webHostEnviroment;
        private readonly string _connectionString;
        public Guid objId;
        private ISession _session;
        SOTSClient sOTSClient;
        #endregion

        #region SECTION INITIALIZE CONNECTIONS
        public ISession Session { get { return _session; } }
        public BudgetController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            _session = httpContextAccessor.HttpContext.Session;
            _connectionString = httpContextAccessor.HttpContext.Session.GetString("DynamicConnection");
        }

        public class Perioddates
        {
            public DateTime FDt { get; set; }
            public DateTime ToDt { get; set; }
        }

        #endregion

        #region SECTION COMBOS
        public void SalesrepCombo(string salesrepname = null)
        {
            var query = "";
            List<SalesRapCombo> salesrepcombo = new List<SalesRapCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            if (salesrepname != null)
            {
                query = sOTSClient.Select_Query_SalesRep_Combo(salesrepname);
                salesrepcombo = sOTSClient.Get_Data_By_Salesrep_Combo(query);
            }
            else
            {
                query = sOTSClient.Select_Query_SalesRep_Combo();
                salesrepcombo = sOTSClient.Get_Data_By_Salesrep_Combo(query);
                salesrepcombo.Insert(0, new SalesRapCombo { Name = "- Select Sales Rep - ", Id = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            }
            ViewBag.salesrepcombolist = salesrepcombo;
        }

        public void ProductCombo()
        {
            List<ProductCombo> productcombo = new List<ProductCombo>();

            sOTSClient = new SOTSClient(_connectionString);
            productcombo = sOTSClient.Get_List_Query_Product_Combo();

            productcombo.Insert(0, new ProductCombo { ProductName = "- Select Product - ", ProductId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewData["productcombolist"] = productcombo.ToList();
            ViewData["defaultproduct"] = productcombo.First();
        }

        public void PackageCombo()
        {
            List<PackageCombo> Packagecombo = new List<PackageCombo>();

            sOTSClient = new SOTSClient(_connectionString);
            Packagecombo = sOTSClient.Get_List_Query_Packages_Combo();

            Packagecombo.Insert(0, new PackageCombo { PackageName = "- Select Package - ", PackageId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewData["Packagecombolist"] = Packagecombo.ToList();
            ViewData["defaultPackage"] = Packagecombo.First();
        }

        public void PeriodTypeCombo()
        {
            List<SelectPeriodtypeList> WorkStates = new List<SelectPeriodtypeList>()
            {
                new SelectPeriodtypeList() { Text = "- Select - ", Value = "0" },
                new SelectPeriodtypeList() { Text="Weekly", Value="1"},
                new SelectPeriodtypeList() { Text="Monthly", Value="2"},
                new SelectPeriodtypeList() { Text="Quarterly", Value="3"},
                new SelectPeriodtypeList() { Text="Half Yearly", Value="4"},
                new SelectPeriodtypeList() { Text="Yearly", Value="5"},
                new SelectPeriodtypeList() { Text="Custom Period", Value="6"}
            };
            ViewBag.Periodtypecombolist = WorkStates;
        }

        #endregion

        #region SECTION IACTIONRESULT  
        public IActionResult Budget()
        {
            ProductCombo();
            PackageCombo();
            PeriodTypeCombo();
            string Uname = "";
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                Uname = this.HttpContext.Session.GetString("Uname");
                SalesrepCombo(Uname);
            }
            else
            {
                SalesrepCombo();
            }
            return View("Budget");
        }

        public IActionResult Index()
        {
           
            string Uname = "";
            IEnumerable<Budget> lstbudget = new List<Budget>();
            sOTSClient = new SOTSClient(_connectionString);
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                Uname = this.HttpContext.Session.GetString("Uname");
                lstbudget = (IEnumerable<Budget>)sOTSClient.Get_List_Query_Budget(Uname);
            }
            else
            //(HttpContext.Session.GetString("Role") == "ClientAdmin" || HttpContext.Session.GetString("Role") == "Client")
            {
                lstbudget = (IEnumerable<Budget>)sOTSClient.Get_List_Query_Budget();
            }
            //lstbudget = (IEnumerable<Budget>)sOTSClient.Get_List_Query_Budget();
            return View(lstbudget);

        }

        public IActionResult Edit(Guid pId, [DataSourceRequest] DataSourceRequest request)
        {
            //ViewBag.formName = "";
            //ViewBag.ViewName = "";
            //ViewBag.ControllerName = ""; 

            SalesrepCombo();
            PeriodTypeCombo();
            Budget EditBudget = new Budget();
            EditBudget = EditBudgetById(pId);

            //Budget_Edit(request, pId);
            ViewBag.budid = pId.ToString();
            //ViewBag.formName = "Edit Budget for Sales Rep";
            //ViewBag.ViewName = "Edit";
            //ViewBag.ControllerName = "Budget";
            return View(EditBudget);
        }

        public async Task<IActionResult> View(Guid pId, Guid salesrep ,  DateTime Fromdate, DateTime Todate)
        {
            Decimal TolAmout ; 
            Budget budget = new Budget();
            string para = "where bl.BudgetId = '" + pId + "'";
            budget = EditBudgetById(pId);
            TolAmout = await GetBudgetBalAmountAsync(salesrep, Fromdate, Todate);
            budget.BalAmount = budget.BudAmount - TolAmout; 
            budget.BudgetLineProducts = GetBudgetLineProductlist(para);
            return View(budget);
        }

        private async Task<decimal> GetBudgetBalAmountAsync(Guid salesrep, DateTime fromdate, DateTime todate )
        {
            try
            {
                decimal BalAmount;
                var parameters = new { Sales = salesrep, Fromdate = fromdate , Todate = todate };
                var sql = "select COALESCE(sum(sp.TOTAL), 0) as BalAmount " +
                         "from Sample.SalesOrderRequest so  " +
                         "inner join Sample.SalesOrderRequestProducts sp on sp.SalesorderRequestId = so.Id " +
                         "full outer join Sample.Budget b on b.SalesRepID = so.SalesRepID " +
                         "where so.IsDeleted = 0 and " +
                         "so.Active = 1 and " +
                         "so.Status = 2 and " +
                         "b.FromPeriod =  @Fromdate and b.ToPeriod = @Todate and " +
                         "so.TxnDate between @Fromdate  and @Todate and " +
                         "b.Active = 1 and b.IsDeleted = 0  and " +
                         "so.SalesRepID = @Sales";
                using (var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();
                    BalAmount = await connection.ExecuteScalarAsync<decimal>(sql, parameters);
                }
                return BalAmount ; 
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }
        public IActionResult Delete(Guid pId)
        {
            if (pId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                DeleteBudget(pId);
            }
            return RedirectToAction("Index", "Budget");
        }
        public IActionResult GetNextbudget([FromBody] Perioddates o)
        // public IActionResult GetNextbudget(string FDt, string ToDt )
        {
            IEnumerable<Budget> lstbudget = new List<Budget>();
            sOTSClient = new SOTSClient(_connectionString);
            string Sql = "select b.FromPeriod , b.id, b.ToPeriod , t.Name as SalesRepName,  b.BudAmount , b.Recurring ," +
                         "b.SalesRepID, b.PeriodType , b.ClientId " +
                         " ,CASE " +
                         "WHEN b.PeriodType = 1 THEN 'Weekly'" +
                         "WHEN b.PeriodType = 2 THEN 'Monthly' " +
                         "WHEN b.PeriodType = 3 THEN 'Quarterly' " +
                         "WHEN b.PeriodType = 5 THEN 'Half Yearly' " +
                         "WHEN b.PeriodType = 4 THEN 'Yearly' " +
                         "ELSE 'Custom Period' " +
                         "END AS PeriodTypeName " +
                         "from  Sample.Budget b " +
                         "inner join Territories t on t.id = b.SalesRepID " +
                         "where b.IsDeleted = 0 and b.Active = 1 " +
                         " and b.ToPeriod < CONVERT(date, GETDATE()) " + 
                         " and b.FromPeriod between '" + o.FDt + "' and '" + o.ToDt + "'";
            
            lstbudget = (List<Budget>)sOTSClient.ListReader<Budget>(Sql, _connectionString);
            return new Microsoft.AspNetCore.Mvc.JsonResult(lstbudget);

            // return Json(lstbudget, JsonRequestBehavior.AllowGet);
            // return new Microsoft.AspNetCore.Mvc.JsonResult(JsonConvert.SerializeObject(lstbudget), JsonRequestBehavior.AllowGet);
            //  string json = JsonConvert.SerializeObject(lstbudget, Formatting.Indented);
            // result = Json(JsonConvert.SerializeObject(data), JsonRequestBehavior.AllowGet);
            // Return info.  
            // return (IActionResult)lstbudget;
        }

        public IActionResult Recurring()
        {
            SalesrepCombo();
            PeriodTypeCombo();

            //    List<Budget> Recurringbudget = new List<Budget>();
            //    sOTSClient = new SOTSClient(_connectionString);
            //    string Sql = "select b.FromPeriod , b.id, b.ToPeriod , t.Name as SalesRepName,  b.BudAmount , b.Recurring ," +
            //                 "b.SalesRepID, b.PeriodType , b.ClientId " +
            //                 " ,CASE " +
            //                 "WHEN b.PeriodType = 1 THEN 'Weekly'" +
            //                 "WHEN b.PeriodType = 2 THEN 'Monthly' " +
            //                 "WHEN b.PeriodType = 3 THEN 'Quarterly' " +
            //                 "WHEN b.PeriodType = 5 THEN 'Half Yearly' " +
            //                 "WHEN b.PeriodType = 4 THEN 'Yearly' " +
            //                 "ELSE 'Custom Period' " +
            //                 "END AS PeriodTypeName " +
            //                 "from  Sample.Budget b " +
            //                 "inner join Territories t on t.id = b.SalesRepID " +
            //                 "where b.IsDeleted = 0 and b.Active = 1 ";
            //                 //"ToPeriod < CONVERT(date, GETDATE())   and Recurring = 1 ";
            //    Recurringbudget = (List<Budget>)sOTSClient.ListReader<Budget>(Sql, _connectionString);
            //    return View(Recurringbudget);
            return View();
        }



        //public IActionResult Recurring_Read([DataSourceRequest] DataSourceRequest request)
        //{
          
        //    {
        //        List<Budget> Recurringbudget = new List<Budget>();
        //        sOTSClient = new SOTSClient(_connectionString);
        //        string Sql = "select b.FromPeriod , b.id, b.ToPeriod , t.Name as SalesRepName,  b.BudAmount , b.Recurring ," +
        //                     "b.SalesRepID, b.PeriodType , b.ClientId " +
        //                     " ,CASE " +
        //                     "WHEN b.PeriodType = 1 THEN 'Weekly'" +
        //                     "WHEN b.PeriodType = 2 THEN 'Monthly' " +
        //                     "WHEN b.PeriodType = 3 THEN 'Quarterly' " +
        //                     "WHEN b.PeriodType = 5 THEN 'Half Yearly' " +
        //                     "WHEN b.PeriodType = 4 THEN 'Yearly' " +
        //                     "ELSE 'Custom Period' " +
        //                     "END AS PeriodTypeName " +
        //                     "from  Sample.Budget b " +
        //                     "inner join Territories t on t.id = b.SalesRepID " +
        //                     "where ToPeriod < CONVERT(date, GETDATE())  and b.IsDeleted = 0 and b.Active = 1 " +
        //                     "and Recurring = 1 ";
        //        Recurringbudget = (List<Budget>)sOTSClient.ListReader<Budget>(Sql, _connectionString);
        //        DataSourceResult result = Recurringbudget.ToDataSourceResult(request, p => new Models.Budget
        //        {
        //            Id = p.Id,
        //            FromPeriod = p.FromPeriod,
        //            ToPeriod = p.ToPeriod,
        //            SalesRepName = p.SalesRepName,
        //            BudAmount = p.BudAmount,
        //            ClientId = p.ClientId,
        //            PeriodType = p.PeriodType,
        //            PeriodRange = p.PeriodRange,
        //            SalesRepID = p.SalesRepID,

        //         });
        //         return Json(result);
                
        //    }
        //}

        //public IActionResult Recurring_Update([DataSourceRequest] DataSourceRequest request, Budget budget)
        //{
        //    if (budget != null && ModelState.IsValid)
        //    {
        //        Guid id = budget.Id;  
        //    }
        //    return Json(new[] { budget }.ToDataSourceResult(request, ModelState));
        //}

        public async Task<IActionResult> UpdateRecurringAsync([FromBody] Budget Recurringbud)
        {
            List<Budget> Recurringbudget = new List<Budget>();
            try
            {
                if (Recurringbud != null && ModelState.IsValid)
                {
                    List<string> Ids = Recurringbud.SalesRepIDs?.Split(',').ToList();
                    foreach (var id in Ids)
                    {
                        Recurringbudget.Add(new Budget
                        {
                            SalesRepID = Guid.Parse(id),
                            PeriodType = Recurringbud.PeriodType,
                            FromPeriod = Recurringbud.FromPeriod,
                            ToPeriod = Recurringbud.ToPeriod,
                            PeriodRange = Recurringbud.PeriodRange,
                            BudAmount = Recurringbud.BudAmount,
                            InsertAgent = Recurringbud.InsertAgent, 

                            Id = Guid.NewGuid(),
                            Inserted = DateTime.UtcNow,
                        });
                    }
                    await PostNextbudget(Recurringbudget);
                    // InsertRecurringBudget(Recurringbud);
                }
            }
            catch (Exception ex)
            {
                throw(ex);
            }

            return new Microsoft.AspNetCore.Mvc.JsonResult(1);
        }

        
        #endregion

        #region SECTION IACTIONRESULT CRUD OPERATION 
        public IActionResult Budget_Edit([DataSourceRequest] DataSourceRequest request, Guid Id)
        {
            try
            {
                List<BudgetLineProducts> list = new List<BudgetLineProducts>();
                string para = "where bl.BudgetId = '" + Id + "'";
                list = GetBudgetLineProductlist(para);
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.BudgetLineProducts
                {
                    Id = p.Id,
                    BudgetId = p.BudgetId,
                    Description = p.Description,
                    Bin = p.Bin,
                    SKU = p.SKU,
                    ClientId = p.ClientId,
                    ProductID = p.ProductID,
                    ProductName = p.ProductName, 
                    PackageId = p.PackageId,
                    PackageName = p.PackageName,
                    Cost = p.Cost,
                    Quantity = p.Quantity,
                    Rebate = p.Rebate,
                    DelivPrice = p.DelivPrice,
                    TOTAL = ((p.Cost * ((100 - p.Rebate) / 100)) * p.Quantity),
                });
                // return Json(result.ToDataSourceResult(request));
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public IActionResult UpdateCreateDelete1Async([FromBody]Budget budget)
        {
            try
            {
                if (budget != null && ModelState.IsValid)
                {
                    var results = new List<BudgetLineProducts>();
                    if (budget.Id == null || budget.Id == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        Guid obj = Guid.NewGuid();
                        objId = obj;
                        budget.Id = objId;
                    }
                    else
                    {
                        objId = budget.Id;
                    }
                    if (budget.BudgetLineProducts != null && ModelState.IsValid)
                    {
                        budget.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                        InsertBudget(budget);

                        foreach (var bugline in budget.BudgetLineProducts)
                        {
                            bugline.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            bugline.Inserted = DateTime.UtcNow;
                            bugline.BudgetId = objId;
                            // call method for insert data 
                            InsertBudgetLine(bugline);
                            results.Add(bugline);
                        }
                    }
                }
                if (budget.Id != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                {
                    ViewBag.budgetid = budget.Id;
                }
                return new Microsoft.AspNetCore.Mvc.JsonResult(ViewBag.budgetid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public SampleOrders DeleteBudget(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    string updateQuery = @"update Sample.Budget set Active = 0 , IsDeleted = 1 where id = '" + id.ToString() + "'";
                    db.Execute(updateQuery);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IActionResult InsertBudget(Budget budgethdr)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", budgethdr.Id);
                        paramDetails.Add("@ClientId", budgethdr.ClientId);
                        paramDetails.Add("@SalesRepID", budgethdr.SalesRepID);
                        paramDetails.Add("@FromPeriod", budgethdr.FromPeriod);
                        paramDetails.Add("@ToPeriod", budgethdr.ToPeriod);
                        paramDetails.Add("@PeriodType", budgethdr.PeriodType);
                        paramDetails.Add("@BudAmount", budgethdr.BudAmount);
                        paramDetails.Add("@Recurring", budgethdr.Recurring);
                        paramDetails.Add("@InsertAgent", budgethdr.InsertAgent);
                        paramDetails.Add("@PeriodRange", budgethdr.PeriodRange);

                        _cn.Execute("Sample.Budgethdr", paramDetails, null, 0, CommandType.StoredProcedure);

                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }


        public IActionResult InsertBudgetLine(BudgetLineProducts budgeline)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", budgeline.Id);
                        paramDetails.Add("@ClientId", budgeline.ClientId);
                        paramDetails.Add("@BudgetId", budgeline.BudgetId);
                        paramDetails.Add("@ProductID", budgeline.ProductID);
                        paramDetails.Add("@ProductName", budgeline.ProductName);
                        paramDetails.Add("@PackageId", budgeline.PackageId);
                        paramDetails.Add("@Quantity", budgeline.Quantity);
                        paramDetails.Add("@Cost", budgeline.Cost);
                        paramDetails.Add("@TOTAL", budgeline.TOTAL);
                        paramDetails.Add("@Rebate", budgeline.Rebate);
                        paramDetails.Add("@DelivPrice", budgeline.DelivPrice);
                        paramDetails.Add("@InsertAgent", budgeline.InsertAgent);

                        _cn.Execute("Sample.BudgelineProduct", paramDetails, null, 0, CommandType.StoredProcedure);
                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }
        public IActionResult ProductModal_Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                List<BudgetLineProducts> list = new List<BudgetLineProducts>();
                sOTSClient = new SOTSClient(_connectionString);
                list = sOTSClient.Get_List_Query_Product_Modal_Budget();
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.BudgetLineProducts
                {
                    Id = p.Id,
                    ProductID = p.ProductID,
                    ProductName = p.ProductName,
                    PackageId = p.PackageId,
                    PackageName = p.PackageName,
                    Cost = p.Cost,
                    Quantity = p.Quantity,
                    Rebate = p.Rebate,
                    DelivPrice = p.DelivPrice
                });
                // return Json(result.ToDataSourceResult(request));
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region DB Function for Get Data & Combo fill
        public List<BudgetLineProducts> GetBudgetLineProductlist(string paraid)
        {
            try
            {
                string sql = "Select bl.Id,  p.Name as Description, p.ManufacturerPartNumber as Bin, p.SKU, bl.ClientId, bl.BudgetId, bl.ProductID, bl.ProductName, bl.Quantity, " +
                      " bl.Cost, (bl.Quantity * ( bl.Cost *  ((100 - bl.Rebate)/100))) as TOTAL, bl.Rebate, bl.DelivPrice, bl.PackageId, pck.Name as PackageName " +
                      " from Sample.BudgetLineProducts bl " +
                      " inner join Products p on p.id = bl.ProductID" +
                      " inner join Packages pck on pck.id = bl.PackageId " + paraid;
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<BudgetLineProducts>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Budget EditBudgetById(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<Budget>("select b.*, t.name as SalesRepName , " +
                        " CASE " +
                        " WHEN PeriodType = 1  THEN  'Weekly' " +
                        " WHEN PeriodType = 2  THEN  'Monthly' " +
                        " WHEN PeriodType = 3  THEN  'Quarterly' " +
                        " WHEN PeriodType = 4  THEN  'Half Yearly' " +
                        " WHEN PeriodType = 5  THEN  'Yearly' " +
                        " ELSE 'Custom Period' END as PeriodTypeName " +
                        " from Sample.Budget b " + 
                        " inner join Territories t on t.id = b.SalesRepID " +
                        " where b.id = '" + id.ToString() + "'").First();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //public IActionResult InsertRecurringBudget(Budget rucbud)
        //{
        //    try
        //    { 
        //        SqlConnection _cn = new SqlConnection(_connectionString);
        //        using (var ts = new TransactionScope())
        //        {
        //            using (_cn)
        //            {
        //                _cn.Open();

        //                var paramDetails = new DynamicParameters();
        //                paramDetails.Add("@Id", rucbud.Id);
        //                paramDetails.Add("@NewFromPeriod", rucbud.FromPeriod);
        //                paramDetails.Add("@NewToPeriod", rucbud.ToPeriod);
        //                paramDetails.Add("@NewPeriodType", rucbud.PeriodType);
        //                paramDetails.Add("@NewPeriodRange", rucbud.PeriodRange);
        //                _cn.Execute("Sample.UpdateRecurringBudget", paramDetails, null, 0, CommandType.StoredProcedure);

        //                ts.Complete();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //    return null;
        //}
        public async Task<IActionResult> PostNextbudget(IEnumerable<Budget> model)
        {
            try
            {
                int check;
                Budget GetRecord = new Budget();
                using (var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();
                    foreach (var bmodel in model)
                    {

                        var sql = "select count(*) from Sample.Budget where Active = 1 and IsDeleted = 0 and SalesRepID = @p1 and PeriodType = @p2 and " +
                                  "FromPeriod = @p3 and ToPeriod = @p4 and PeriodRange = @p5 ";

                        var Latestbudget = "select top 1 * from Sample.Budget where Active = 1 and IsDeleted = 0 and SalesRepID = @p1 order by Inserted desc";

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@p1", bmodel.SalesRepID);
                        paramDetails.Add("@p2", bmodel.PeriodType);
                        paramDetails.Add("@p3", bmodel.FromPeriod);
                        paramDetails.Add("@p4", bmodel.ToPeriod);
                        paramDetails.Add("@p5", bmodel.PeriodRange);

                        var oldbudget = await connection.QueryAsync<Budget>(Latestbudget, paramDetails);

                        check = await connection.ExecuteScalarAsync<int>(sql, paramDetails);

                        foreach (var old in oldbudget)
                        {
                            GetRecord.Id = Guid.NewGuid();
                            GetRecord.ClientId = old.ClientId;
                            GetRecord.SalesRepID = bmodel.SalesRepID;
                            GetRecord.FromPeriod = bmodel.FromPeriod;
                            GetRecord.ToPeriod = bmodel.ToPeriod;
                            GetRecord.PeriodType = bmodel.PeriodType;
                            GetRecord.BudAmount = old.BudAmount;
                            GetRecord.Recurring = old.Recurring;
                            GetRecord.Active = old.Active;
                            GetRecord.Inserted = bmodel.Inserted;
                            GetRecord.InsertSource = old.InsertSource;
                            GetRecord.InsertAgent = old.InsertAgent;
                            GetRecord.PeriodRange = bmodel.PeriodRange;
                            GetRecord.Updated = bmodel.Inserted;
                            GetRecord.UpdateSource = old.UpdateSource;
                            GetRecord.UpdateAgent = old.UpdateAgent;
                        }

                        if (check == 0)
                        {
                            var insertnextbudget = @" INSERT INTO [Sample].[Budget]         
                                ( [Id] , [ClientId], [SalesRepID], [FromPeriod], [ToPeriod],      
                                  [PeriodType] , [BudAmount], [Recurring], [Active],      
                                  [Inserted], [InsertSource], [InsertAgent], [PeriodRange],
                                  [Updated],[UpdateSource],[UpdateAgent]) 
                            VALUES (
                                     @Id  
                                    ,@ClientId 
                                    ,@SalesRepID  
                                    ,@FromPeriod 
                                    ,@ToPeriod 
                                    ,@PeriodType  
                                    ,@BudAmount   
                                    ,@Recurring  
                                    ,@Active 
                                    ,@Inserted 
                                    ,@InsertSource
                                    ,@InsertAgent 
                                    ,@PeriodRange 
                                    ,@Updated 
                                    ,@UpdateSource
                                    ,@UpdateAgent 
                                    )";
                            await connection.ExecuteAsync(insertnextbudget, GetRecord);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }

        #endregion
    }
}
