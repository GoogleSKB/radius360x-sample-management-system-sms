﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleOrdersTrackingSystem.Models;
using System.Web.Mvc;
using BindAttribute = Microsoft.AspNetCore.Mvc.BindAttribute;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SampleOrdersTrackingSystem.Extensions;
using SelectPeriodtypeList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using SOTSLibrary;
using SampleOrdersTrackingSystem.Models.Dropdown;
using SOTSLibrary.DataBindingModel;
using System.Transactions;
using CommandType = System.Data.CommandType;

namespace SampleOrdersTrackingSystem.Controllers
{
    public class PurchaseOrderController : Controller
    {

        #region SECTION GLOBAL VARS
        private readonly IWebHostEnvironment _webHostEnviroment;
        private readonly string _connectionString;
        public Guid objId;
        private ISession _session;
        SOTSClient sOTSClient;
        #endregion

        #region SECTION INITIALIZE CONNECTIONS
        public ISession Session { get { return _session; } }

        public PurchaseOrderController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _session = httpContextAccessor.HttpContext.Session;
        }

        #endregion

        #region SECTION COMBOS
        public void BINDSHIPVIA()
        {
            List<ShipviaCombo> ShipVia = new List<ShipviaCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_ShipMethods();
            ShipVia = sOTSClient.Get_Data_By_Shipvia_Combo(query);

            ShipVia.Insert(0, new ShipviaCombo { Shipvia = "- Select Ship via - ", ShipMethodId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewBag.shipViaComboList = ShipVia;
        }


        public void BINDCUSTOMTERMS()
        {
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_CustomerTerms();
            List<CommonDropdown> customerTerms = new List<CommonDropdown>();
            customerTerms = sOTSClient.Get_Data_By_Query(query);
            customerTerms.Insert(0, new CommonDropdown { Name = "- Select Customer Terms - ", Id = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewBag.customTermsComboList = customerTerms;
        }

        public void BINDVENDORS()
        {
            List<VendorCombo> vendorCombo = new List<VendorCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_Vendor();
            vendorCombo = sOTSClient.Get_Data_Vendor(query);

            vendorCombo.Insert(0, new VendorCombo { VendorName = "- Select Vendor - ", VendorId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewBag.vendorComboList = vendorCombo;
        }

        #endregion

        #region SECTION IACTIONRESULT   
        public IActionResult Index()
        {
            ViewBag.formName = "";
            ViewBag.ViewName = "";
            ViewBag.ControllerName = "";
            sOTSClient = new SOTSClient(_connectionString);
            IEnumerable<PurchaseOrders> lstbudget = new List<PurchaseOrders>();
            lstbudget = (IEnumerable<PurchaseOrders>)sOTSClient.GetPurchaseOrdersList(Guid.Empty);
            ViewBag.formName = "Purchase Order";
            ViewBag.ViewName = "Index";
            ViewBag.ControllerName = "PurchaseOrder";
            return View(lstbudget);
        }

        //public IActionResult PurchaseOrder()
        //{
        //    ViewBag.formName = "";
        //    ViewBag.ViewName = "";
        //    ViewBag.ControllerName = "";
        //    BINDSHIPVIA();
        //    BINDCUSTOMTERMS();
        //    BINDVENDORS();
        //    ViewBag.formName = "Create New Purchase Order";
        //    ViewBag.ViewName = "Purchase Order";
        //    ViewBag.ControllerName = "Purchase Order";
        //    return View();
        //}
        public IActionResult Edit(Guid pId, [DataSourceRequest] DataSourceRequest request)
        {
            ViewBag.formName = "";
            ViewBag.ViewName = "";
            ViewBag.ControllerName = "";
            BINDSHIPVIA();
            BINDCUSTOMTERMS();
            PurchaseOrders editPO = new PurchaseOrders();
            if (pId != null || pId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {

                var hdrquery = sOTSClient.GetPurchaseOrders(pId);
                editPO = (PurchaseOrders)sOTSClient.FirstOrDefault<PurchaseOrders>(hdrquery, _connectionString);
            }

            //List<PurchaseOrderLineItems> list = new List<PurchaseOrderLineItems>();
            //var query = sOTSClient.GetPurchaseOrders(pId);
            //list = (List<PurchaseOrderLineItems>)sOTSClient.ListReader<PurchaseOrderLineItems>(query, _connectionString);
            
            ViewBag.vendorId = pId.ToString();
            ViewBag.formName = "Generate Purchase Orders";
            ViewBag.ViewName = "Edit";
            ViewBag.ControllerName = "Purchase Order";
            return View(editPO);
        }


        public IActionResult UpdateCreateDelete([FromBody] PurchaseOrders purchase)
         {
            try
            {
                if (purchase != null && ModelState.IsValid)
                {
                    var results = new List<PurchaseOrderLineItems>();
                    
                    if (purchase.Id == null || purchase.Id == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        Guid obj = Guid.NewGuid();
                        objId = obj;
                        purchase.Id = objId;
                    }
                    else
                    {
                        objId = purchase.Id;
                    }
                    purchase.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                     Insertproduct(purchase);
                    foreach (var poline in purchase.PurchaseOrderLineItems)
                    {
                        if (poline != null && ModelState.IsValid)
                        {
                            poline.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            poline.PurchaseOrderId = objId;

                            // call method for insert data 
                            InsertProductline(poline);
                            results.Add(poline);
                        }
                    }
                }
                return RedirectToAction("Index", "PurchaseOrder");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IActionResult Insertproduct(PurchaseOrders purchase)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", purchase.Id);
                        paramDetails.Add("@VendorId", purchase.VendorId);
                        paramDetails.Add("@TermsId", purchase.TermsId);
                        paramDetails.Add("@ShipMethodId", purchase.ShipMethodId);
                        paramDetails.Add("@Date", purchase.Date);
                        paramDetails.Add("@DueDate", purchase.DueDate);
                        paramDetails.Add("@InsertAgent", purchase.InsertAgent);

                        _cn.Execute("Sample.PurchaseOrderhdr", paramDetails, null, 0, CommandType.StoredProcedure);
                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }

        public IActionResult InsertProductline(PurchaseOrderLineItems poline)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", poline.Id);
                        paramDetails.Add("@PurchaseOrderId", poline.PurchaseOrderId);
                        paramDetails.Add("@ProductId", poline.ProductId);
                        paramDetails.Add("@CustomerId", poline.CustomerId);
                        paramDetails.Add("@Quantity", poline.Quantity);
                        paramDetails.Add("@Price", poline.Price);
                        paramDetails.Add("@InsertAgent", poline.InsertAgent);

                        _cn.Execute("Sample.PurchaseOrderLineItems", paramDetails, null, 0, CommandType.StoredProcedure);

                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }


        public IActionResult PurchaseOrder_Edit([DataSourceRequest] DataSourceRequest request, Guid Id)
        {
            try
            {
                List<PurchaseOrderLineItems> list = new List<PurchaseOrderLineItems>();
                string VendorId = Id.ToString();
                sOTSClient = new SOTSClient(_connectionString);
                list = sOTSClient.GetPurchaseOrderLineItemsList(VendorId);
                DataSourceResult result = list.ToDataSourceResult(request, p => new Models.PurchaseOrderLineItems
                {
                    Id = p.Id,
                    PurchaseOrderId = p.PurchaseOrderId,
                    CustomerId = p.CustomerId,
                    SKU = p.SKU,
                    Description = p.Description,
                    Price = p.Price,
                    Quantity = p.Quantity,
                 
                });
                 
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

 
        #endregion

    }
}