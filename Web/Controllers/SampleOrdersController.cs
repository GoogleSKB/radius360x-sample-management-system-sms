﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SampleOrdersTrackingSystem.Models;
using System.Web.Mvc;
using BindAttribute = Microsoft.AspNetCore.Mvc.BindAttribute;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SampleOrdersTrackingSystem.Extensions;

using Controller = Microsoft.AspNetCore.Mvc.Controller;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Transactions;
using CommandType = System.Data.CommandType;
using SOTSLibrary;
using SampleOrdersTrackingSystem.Models.Dropdown;
using SOTSLibrary.DataBindingModel;
using SelectSampleReasonList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;
using SelectBranchList = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;

namespace SampleOrdersTrackingSystem.Controllers
{
    public class SampleOrdersController : Controller
    {

        #region SECTION GLOBAL VARS
        private readonly IWebHostEnvironment _webHostEnviroment;
        private static string _connectionString = null;
        public Guid objId;
        private readonly ISession _session;

        SOTSClient sOTSClient;
        #endregion

        #region SECTION INITIALIZE CONNECTIONS
        public ISession Session { get { return _session; } }

        public SampleOrdersController(IWebHostEnvironment webHostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
            _session = httpContextAccessor.HttpContext.Session;
            _connectionString = httpContextAccessor.HttpContext.Session.GetString("DynamicConnection");
        }

        #endregion

        #region SECTION COMBOS

        public void ProductCombo()
        {
            List<ProductCombo> productcombo = new List<ProductCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_Product_Combo();
            productcombo = sOTSClient.Get_Data_By_Product_Combo(query);
            productcombo.Insert(0, new ProductCombo { ProductName = "- Select Product - ", ProductId = Guid.Parse("00000000-0000-0000-0000-000000000000") });

            ViewData["productcombolist"] = productcombo.ToList();
            ViewData["defaultproduct"] = productcombo.First();

        }
        public void SalesrepCombo(string salesrepname = null)
        {
            var query = ""; 
            List<SalesRapCombo> salesrepcombo = new List<SalesRapCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            if (salesrepname != null)
            {
                query = sOTSClient.Select_Query_SalesRep_Combo(salesrepname);
                salesrepcombo = sOTSClient.Get_Data_By_Salesrep_Combo(query);
            }
            else
            {
                query = sOTSClient.Select_Query_SalesRep_Combo();
                salesrepcombo = sOTSClient.Get_Data_By_Salesrep_Combo(query);
                salesrepcombo.Insert(0, new SalesRapCombo { Name = "- Select Sales Rep - ", Id = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            }
            ViewBag.salesrepcombolist = salesrepcombo;
        }
        public void CustomerCombo()
        {
            List<Customers> Customercombo = new List<Customers>();
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_Customer_Combo();
            Customercombo = sOTSClient.Get_Data_By_Customer_Combo(query);
            Customercombo.Insert(0, new Customers { CustomerName = "- Select Product - ", CustomerId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewData["customercombolist"] = Customercombo.ToList();
            ViewData["defaultcustomer"] = Customercombo.First();
        }
        public void DriverCombo()
        {
            List<DriverCombo> Drivercombo = new List<DriverCombo>();
            sOTSClient = new SOTSClient(_connectionString);
            var query = sOTSClient.Select_Query_Driver_Combo();
            Drivercombo = (List<DriverCombo>)sOTSClient.ListReader<DriverCombo>(query, _connectionString);
            Drivercombo.Insert(0, new DriverCombo { DriverName = "- Select Driver - ", DriverId = Guid.Parse("00000000-0000-0000-0000-000000000000") });
            ViewData["drivercombolist"] = Drivercombo.ToList();
            ViewData["defaultdriver"] = Drivercombo.First();
        }
        public void SampleReasonCombo()
        {
            List<SelectSampleReasonList> Reasons = new List<SelectSampleReasonList>()
            {
                new SelectSampleReasonList() { Text = "- Select - ", Value = "0" },
                new SelectSampleReasonList() { Text="By the Glass Opportunity", Value="1"},
                new SelectSampleReasonList() { Text="Charity Even", Value="2"},
                new SelectSampleReasonList() { Text="Market Sample", Value="3"},
                new SelectSampleReasonList() { Text="New BTG Opportunity", Value="4"},
                new SelectSampleReasonList() { Text="New Wine List", Value="5"},
                new SelectSampleReasonList() { Text="New Product Launch", Value="6"},
                new SelectSampleReasonList() { Text="New Placement Opportunity", Value="7"},
                new SelectSampleReasonList() { Text="Staff Training", Value="8"},
                new SelectSampleReasonList() { Text="Supplier Ride with", Value="9"},
                new SelectSampleReasonList() { Text="Tasting", Value="10"},
                new SelectSampleReasonList() { Text="Wine Dinner", Value="11"}
                //new SelectSampleReasonList() { Text="Other (Please Specify)", Value="12"}
            };
            ViewBag.Resonelistcombo = Reasons;
        }

        //public void BranchCombo()
        //{
        //    List<SelectBranchList> Branch = new List<SelectBranchList>()
        //    {
        //        new SelectBranchList() { Text ="- Select - ", Value = "0" },
        //        new SelectBranchList() { Text="SC", Value="1"},
        //        new SelectBranchList() { Text="NC", Value="2"}
        //    };
        //    ViewBag.Branchlistcombo = Branch;
        //}

        public SampleOrders EditSampleById(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SampleOrders>("Select s.*, d.Name as DriverName , t.name as SalesRepName from Sample.SalesOrderRequest s " +
                        " inner join Territories t on t.id = s.SalesRepID " +
                        " left join Drivers d on d.id = s.DriverId " +
                        "where s.id = '" + id.ToString() + "'").First();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region SECTION IACTIONRESULT   
        public IActionResult SampleOrders()
        {
            ProductCombo();
            SampleReasonCombo();
            DriverCombo();
             
            string Uname = "";
            Uname = this.HttpContext.Session.GetString("Uname");
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                SalesrepCombo(Uname);
            }
            else
            {
                SalesrepCombo();
                ViewBag.territoryid = "";
            }
            ViewBag.territoryid = Uname;
            ViewBag.ViewName = "Sample Orders";
            ViewBag.ControllerName = "SampleOrder";
            return View();
        }
        public IActionResult Index()
        {
            //ViewBag.formName = "";
            //ViewBag.ViewName = "";
            //ViewBag.ControllerName = "";
            string Uname = "";
            sOTSClient = new SOTSClient(_connectionString);
            IEnumerable<SampleOrders> lstsample = new List<SampleOrders>();
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                Uname = this.HttpContext.Session.GetString("Uname");
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(Uname, "1");
            }
            else if(HttpContext.Session.GetString("Role") == "ClientAdmin" || HttpContext.Session.GetString("Role") == "Client")
            {
               lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "1");
            }
            else
            {
                lstsample = (IEnumerable<SampleOrders>)sOTSClient.GetSampleorderlist(null, "0");
               // ViewBag.vmessage = "You have No right for Sales Order Request";
            }
            //ViewBag.formName = "Sample Orders";
            //ViewBag.ViewName = "Index";
            //ViewBag.ControllerName = "Sample Orders";
            return View(lstsample);

        }

        public IActionResult View(Guid pId)
        {
            SampleOrders sample = new SampleOrders();

            string prop = "where  bl.IsDeleted  = 0 and bl.Active = 1 and bl.SalesorderRequestId = '" + pId + "'";
            string custp = " and sc.SalesorderRequestId = '" + pId + "'";

            sample = EditSampleById(pId);
            sample.SalesOrderRequestProducts  = GetSampleProductlist(prop);
            sample.SalesOrderRequestCustomers = GetSampleCustomerlist(custp);
            ViewBag.formName = "Sample Orders";
            ViewBag.ViewName = "Display";
            ViewBag.ControllerName = "Sample Orders";
            return View(sample);
        }

        public IActionResult Delete(Guid pId)
        {
            if(pId != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                DeleteSampleOrders(pId);
            }
            return RedirectToAction("Index", "SampleOrders");
        }

        public IActionResult Edit(Guid pId, [DataSourceRequest] DataSourceRequest request)
        {
            ViewBag.formName = "";
            ViewBag.ViewName = "";
            ViewBag.ControllerName = "";
            string Uname = "";
            Uname = this.HttpContext.Session.GetString("Uname");

            ProductCombo();
            //BranchCombo();
            SampleReasonCombo();
            SalesrepCombo();
            DriverCombo();
            SampleOrders EditSample = new SampleOrders();
            EditSample = EditSampleById(pId);

            ViewBag.sampleid = pId.ToString();
            ViewBag.territoryid = Uname;
             
            ViewBag.formName = "Edit Sample Orders";
            ViewBag.ViewName = "Edit";
            ViewBag.ControllerName = "Sample Orders";
            return View(EditSample);
        }

        #endregion  

        #region SECTION FOR CURD OPERATION

        public IActionResult UpdateCreateDeleteAsync([FromBody] SampleOrders sample)
        {
            try
            {
                if (sample != null && ModelState.IsValid)
                {
                    //var results = new List<SalesOrderRequestProduct>();
                    //var resultscust = new List<SalesOrderRequestCustomer>();
                    if (sample.Id == null || sample.Id == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        Guid obj = Guid.NewGuid();
                        objId = obj;
                        sample.Id = objId;
                    }
                    else
                    {
                        objId = sample.Id;
                    }
                    sample.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                    Insertsample(sample);

                    foreach (var samproduct in sample.SalesOrderRequestProducts)
                    {
                        if (samproduct != null && ModelState.IsValid)
                        {
                            samproduct.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            samproduct.TOTAL = ((samproduct.Cost * ((100 - samproduct.Rebate) / 100)) * samproduct.Quantity);
                            samproduct.SalesorderRequestId = objId;
                            // call method for insert data 
                            InsertSampleProduct(samproduct);
                           // results.Add(samproduct);
                        }
                    }
                    foreach (var delsamproduct in sample.DeleteProducts)
                    {
                        if (delsamproduct != null && ModelState.IsValid)
                        {
                            delsamproduct.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            delsamproduct.SalesorderRequestId = objId;
                            // call method for Delete data 
                             DelSampleProduct(delsamproduct);
                           // results.Add(delsamproduct);
                        }
                    }
                    foreach (var delsamcustomer in sample.DeleteCustomers)
                    {
                        if (delsamcustomer != null && ModelState.IsValid)
                        {
                            delsamcustomer.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            delsamcustomer.SalesorderRequestId = objId;
                            // call method for Delete data 
                             DelSamplecustomer(delsamcustomer);
                           // resultscust.Add(delsamcustomer);
                        }
                    }

                    foreach (var samcustomer in sample.SalesOrderRequestCustomers)
                    {
                        if (samcustomer != null && ModelState.IsValid)
                        {
                            samcustomer.InsertAgent = Convert.ToString(HttpContext.Session.GetString("Uname"));
                            // samcustomer.Inserted = DateTime.UtcNow;
                            samcustomer.SalesorderRequestId = objId;
                            // call method for insert data 
                             InsertSampleCustomer(samcustomer);
                        }
                    }
                }
                if (sample.Id != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                {
                    ViewBag.sampleid = sample.Id;
                }
                return new Microsoft.AspNetCore.Mvc.JsonResult(ViewBag.sampleid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IActionResult GetBudgetAmount([FromBody] SalesRapCombo Salesrep)
        {
            string resultmsg = "";
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var p = new DynamicParameters();
                p.Add("salesrepid", Salesrep.Id);
                p.Add("retstr", "", direction: ParameterDirection.Output);
                db.Query<string>("GetBudgetbySalesrepId", p, commandType: CommandType.StoredProcedure);
                var returnCode = p.Get<string>("retstr");
                if (returnCode !=  null) { 
                    resultmsg = returnCode;
                }
                else
                {
                    resultmsg = "0";
                }
            }
            return new Microsoft.AspNetCore.Mvc.JsonResult(resultmsg);
        }

    public IActionResult Sample_ProductEdit([DataSourceRequest] DataSourceRequest request, Guid Id)
        {
            try
            {
                List<SalesOrderRequestProduct> productlist = new List<SalesOrderRequestProduct>();
                string productpara = "where bl.Active = 1 and bl.IsDeleted = 0 and bl.SalesorderRequestId = '" + Id + "'";
                productlist = GetSampleProductlist(productpara);
                DataSourceResult result = productlist.ToDataSourceResult(request, p => new Models.SalesOrderRequestProduct
                {
                    Id = p.Id,
                    SalesorderRequestId = p.SalesorderRequestId,
                    SKU = p.SKU,
                    Bin = p.Bin,
                    Description = p.Description,
                    ClientId = p.ClientId,
                    ProductID = p.ProductID,
                    ProductName = p.ProductName,
                    PackageId = p.PackageId,
                    PackageName = p.PackageName,
                    Cost = p.Cost,
                    Quantity = p.Quantity,
                    Rebate = p.Rebate,
                    DelivPrice = p.DelivPrice,
                    AvaliableQty = p.AvaliableQty,
                    TOTAL =  ((p.Cost * ((100 - p.Rebate)/100)) * p.Quantity) , 
                    ApprovedQty = (p.ApprovedQty != 0) ?  p.ApprovedQty : p.Quantity,  
                    AprovedCost = (p.AprovedCost != 0) ? p.AprovedCost : p.Cost,
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public IActionResult Sample_CustomerEdit([DataSourceRequest] DataSourceRequest request, Guid Id)
        {
            try
            {
                List<SalesOrderRequestCustomer> customerlist = new List<SalesOrderRequestCustomer>();
                string para = " and sc.SalesorderRequestId = '" + Id + "'";
                customerlist = GetSampleCustomerlist(para);
                DataSourceResult result = customerlist.ToDataSourceResult(request, p => new Models.SalesOrderRequestCustomer
                {
                    Id = p.Id,
                    SalesorderRequestId = p.SalesorderRequestId,
                    ClientId = p.ClientId,
                    CustomerId = p.CustomerId,
                    CustomerName = p.CustomerName,
                    ShipAddressId = p.ShipAddressId,
                    ShipAddress = p.ShipAddress
                });
                return Json(result);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message.ToString();
                return Json(errorMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #region Insert/Update Method for DB
        // public IActionResult Insertsample(SampleOrders samplehrd)
        public Task<IActionResult> Insertsample(SampleOrders samplehrd)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();
                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", samplehrd.Id);
                        paramDetails.Add("@ClientId", samplehrd.ClientId);
                        paramDetails.Add("@SalesRepID", samplehrd.SalesRepID);
                        paramDetails.Add("@Status", samplehrd.Status);
                        paramDetails.Add("@TxnDate", samplehrd.TxnDate);
                        paramDetails.Add("@SalesorderTypeID", samplehrd.SalesorderTypeID);
                        paramDetails.Add("@Reasonforsample", samplehrd.Reasonforsample);
                        paramDetails.Add("@Comments", samplehrd.Comments);
                        paramDetails.Add("@InsertAgent", samplehrd.InsertAgent);
                        paramDetails.Add("@DriverId", samplehrd.DriverId);
                        paramDetails.Add("@RequestNo", samplehrd.RequestNo);

                        _cn.Execute("Sample.Samplehdr", paramDetails, null, 0, CommandType.StoredProcedure);
                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }

        public Task<IActionResult> InsertSampleProduct(SalesOrderRequestProduct sampleprd)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", sampleprd.Id);
                        paramDetails.Add("@ClientId", sampleprd.ClientId);
                        paramDetails.Add("@SalesorderRequestId", sampleprd.SalesorderRequestId);
                        paramDetails.Add("@ProductID", sampleprd.ProductID);
                        paramDetails.Add("@ProductName", sampleprd.ProductName);
                        paramDetails.Add("@PackageId", sampleprd.PackageId);
                        paramDetails.Add("@Quantity", sampleprd.Quantity);
                        paramDetails.Add("@Cost", sampleprd.Cost);
                        paramDetails.Add("@TOTAL", sampleprd.TOTAL);
                        paramDetails.Add("@Rebate", sampleprd.Rebate);
                        paramDetails.Add("@DelivPrice", sampleprd.DelivPrice);
                        paramDetails.Add("@InsertAgent", sampleprd.InsertAgent);

                        _cn.Execute("Sample.Sampleproduct", paramDetails, null, 0, CommandType.StoredProcedure);

                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }

        public Task<IActionResult> InsertSampleCustomer(SalesOrderRequestCustomer samplecust)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();

                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", samplecust.Id);
                        paramDetails.Add("@ClientId", samplecust.ClientId);
                        paramDetails.Add("@SalesorderRequestId", samplecust.SalesorderRequestId);
                        paramDetails.Add("@CustomerId", samplecust.CustomerId);
                        paramDetails.Add("@ShipAddressId", samplecust.ShipAddressId);
                        paramDetails.Add("@InsertAgent", samplecust.InsertAgent);

                        _cn.Execute("Sample.SampleCustomer", paramDetails, null, 0, CommandType.StoredProcedure);

                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }
        public Task<IActionResult> DelSampleProduct(SalesOrderRequestProduct delsamplecust)
        {
            try
            {
                SqlConnection _cn = new SqlConnection(_connectionString);
                using (var ts = new TransactionScope())
                {
                    using (_cn)
                    {
                        _cn.Open();
                        var paramDetails = new DynamicParameters();
                        paramDetails.Add("@Id", delsamplecust.Id);
                        paramDetails.Add("@SalesorderRequestId", delsamplecust.SalesorderRequestId);
                        _cn.Execute("Sample.SampleDelProducts", paramDetails, null, 0, CommandType.StoredProcedure);
                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return null;
        }
        public Task<IActionResult> DelSamplecustomer(SalesOrderRequestCustomer delsamplecust)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    string updateQuery = @"update sample.SalesOrderRequestCustomers set IsDeleted = 1, Active    = 0  where SalesorderRequestId = '" + delsamplecust.SalesorderRequestId.ToString() + "'" +  "and id = '" + delsamplecust.Id.ToString() + "'"; 
                    db.Execute(updateQuery);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public SampleOrders DeleteSampleOrders(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    string updateQuery = @"update Sample.SalesOrderRequest set Active = 0 , IsDeleted = 1 where id = '" + id.ToString() + "'";
                    db.Execute(updateQuery);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<SalesOrderRequestProduct> GetSampleProductlist(string productpara)
        {
            try
            {
                string sql = "Select bl.Id, p.Name as Description, p.ManufacturerPartNumber as Bin, p.SKU, bl.SalesorderRequestId, bl.ClientId, bl.SalesorderRequestId, bl.ProductID, p.name as ProductName , bl.Quantity, " +
                      " bl.Cost, bl.TOTAL, bl.Rebate, " +
                      " bl.DelivPrice, bl.PackageId, pck.Name as PackageName, convert(decimal(18,2),ai.QtyOnHand-ai.QtyOnSalesOrder)  as AvaliableQty, " +
                      " bl.ApprovedQty, bl.AprovedCost, bl.StatusId" +
                      " from Sample.SalesOrderRequestProducts bl " +
                      " inner join Products p on p.id = bl.ProductID" +
                      " inner join AccountInventory ai on p.id =  ai.Productid" + 
                      " inner join Packages pck on pck.id = bl.PackageId " + productpara;

                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SalesOrderRequestProduct>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

         
        public List<SalesOrderRequestCustomer> GetSampleCustomerlist(string paraid)
            {
            try
            {
                string sql = "select sc.id,  sc.CustomerId, sc.ShipAddressId, c.name as Customername , c.ShipToAddressId ,  (ISNULL(ADDBL.Extension1, '') + ' ' + ISNULL(ADDBL.extension2, '') + ' ' +  " +
                             " ISNULL(ADDBL.City, '') + ' ' + ISNULL(ADDBL.State, '') + ' ' + ISNULL(ADDBL.PostalCode, '')) ShipAddress " +
                             " from  Sample.SalesOrderRequestCustomers sc " +
                             " inner join customers c  on c.Id = sc.CustomerId " +
                             " inner join Addresses addbl on addbl.id = c.BillToAddressId " +
                             " where sc.IsDeleted  = 0  and sc.Active = 1 " + paraid;

                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<SalesOrderRequestCustomer>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        #endregion


        #endregion

    }
}
