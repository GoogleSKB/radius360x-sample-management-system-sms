﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SampleOrdersTrackingSystem.Models;
using SampleOrdersTrackingSystem.Models.Dropdown;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace SampleOrdersTrackingSystem.Controllers
{
    public class HomeController : Controller
    {
        // private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _webHostEnviroment;
        private readonly string _UMconnectionString ;
        private readonly ISession _session;
          
            
        public HomeController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _webHostEnviroment = webHostEnvironment;
            _UMconnectionString = configuration.GetConnectionString("UMConnection");
            _session = httpContextAccessor.HttpContext.Session;
        }
        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(String Display_Name, String Email, string UserName, string Company_Name, String Role, String ClientID,
                                  String TeamMemberID, int AccType, int RoleForApp, String IsClient, String IsTeamMember, byte IsEmailConfirm,
                                  byte IsAuthenticated, string Response, byte AddPermission, byte EditPermission, int Subscription_Plan, string ServerName)
        {
            List<DynamicConn> dynamicConns = new List<DynamicConn>();
            HttpContext.Session.SetString("Role", Role);
            if (HttpContext.Session.GetString("Role") == "ideskAdmin")
            {

                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", "IdeskHub");
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", "0");
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");

                HttpContext.Session.SetInt32("IsEmailConfirm", IsEmailConfirm);
                HttpContext.Session.SetInt32("IsAuthenticated", IsAuthenticated);
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetInt32("PowerBIReportAdd", AddPermission);
                HttpContext.Session.SetInt32("PowerBIReportEdit", EditPermission);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("ServerName", ServerName);

            }
            if (HttpContext.Session.GetString("Role") == "Client")
            {

                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetInt32("Subscription_Plan", Subscription_Plan);
                HttpContext.Session.SetString("ServerName", ServerName);


            }
            if (HttpContext.Session.GetString("Role") == "ClientAdmin")
            {
                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetInt32("Subscription_Plan", Subscription_Plan);
                HttpContext.Session.SetString("ServerName", ServerName);

                HttpContext.Session.SetString("Uname", UserName);
               
            }
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", TeamMemberID);
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", IsClient);
                HttpContext.Session.SetString("Uname", UserName);

                HttpContext.Session.SetString("IsTeamMember", IsTeamMember);
                HttpContext.Session.SetInt32("IsEmailConfirm", IsEmailConfirm);
                HttpContext.Session.SetInt32("IsAuthenticated", IsAuthenticated);
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetInt32("PowerBIReportAdd", AddPermission);
                HttpContext.Session.SetInt32("PowerBIReportEdit", EditPermission);
                HttpContext.Session.SetString("ServerName", ServerName);
            }


          if (Convert.ToString(HttpContext.Session.GetInt32("RoleId")) == "")
          {
              ViewBag.Message = "You Don't Have Premission To Access Please Contact " + Company_Name + " Admin";
              return View("Index");
          }

          if(HttpContext.Session.GetString("Role") == "Client")
            {
                dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString);
                foreach (var conn in dynamicConns)
                { 
                    if(conn.Islive == true)
                    {
                        HttpContext.Session.SetString("ProductionConn", conn.ConnectionString);
                        HttpContext.Session.SetString("DynamicConnection", conn.ConnectionString);
                        HttpContext.Session.SetString("Connection", "Production");
                    }
                    if(conn.Islive == false)
                    {
                        HttpContext.Session.SetString("DevelopmentConn", conn.ConnectionString);
                       // HttpContext.Session.SetString("Connection", "Development");
                    }
                }
            }
          else
            {
                if (HttpContext.Session.GetString("ServerName") == "apitwo.indigoolive.com")
                {
                    dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString, "1");
                    HttpContext.Session.SetString("Connection", "Production");
                }
                else 
                {
                    dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString, "0");
                    HttpContext.Session.SetString("Connection", "Development");
                }
                foreach(var con in dynamicConns)
                {
                 //--'apitwo.indigoolive.com'  Is live = 1
                 //--'apione.indigoolive.com'  Is live = 0
                  HttpContext.Session.SetString("DynamicConnection", con.ConnectionString);
                }
                 
            }
            return RedirectToAction("Dashboard", "Home");
        }
        public IActionResult Dashboard()
        {
            if (Convert.ToString(HttpContext.Session.GetInt32("RoleId")) == "")
            {
                ViewBag.Message = "You Don't Have Premission To Access Please Contact " + HttpContext.Session.GetString("Company_Name")  + " Admin";
                return View("Index");
            }
            else if (HttpContext.Session.GetString("Role") == "Client")
            {
                return View();
            }
            else
            {
                if (HttpContext.Session.GetString("Role") == "ClientAdmin")
                {
                    return RedirectToAction("Index", "SalesOrderApproval");
                }
                else  
                {
                    return RedirectToAction("Index", "SampleOrders");
                }
            }
        }


        public ActionResult IndexWithoutlogin(String Display_Name, String Email, string UserName, string Company_Name, String Role,
                                              String ClientID, String TeamMemberID, int AccType, int RoleForApp, String IsClient,
                                              String IsTeamMember, byte IsEmailConfirm, byte IsAuthenticated, string Response,
                                              byte AddPermission, byte EditPermission, int Subscription_Plan, string ServerName)
        {
            List<DynamicConn> dynamicConns = new List<DynamicConn>();
            HttpContext.Session.SetString("Role", Role);
            if (HttpContext.Session.GetString("Role") == "ideskAdmin")
            {

                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", "IdeskHub");
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", "0");
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");

                HttpContext.Session.SetInt32("IsEmailConfirm", IsEmailConfirm);
                HttpContext.Session.SetInt32("IsAuthenticated", IsAuthenticated);
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetInt32("PowerBIReportAdd", AddPermission);
                HttpContext.Session.SetInt32("PowerBIReportEdit", EditPermission);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("ServerName", ServerName);

            }
            if (HttpContext.Session.GetString("Role") == "Client")
            {

                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetInt32("Subscription_Plan", Subscription_Plan);
                HttpContext.Session.SetString("ServerName", ServerName);


            }
            if (HttpContext.Session.GetString("Role") == "ClientAdmin")
            {
                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", "0");
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", "0");
                HttpContext.Session.SetString("IsTeamMember", "0");
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetString("Uname", UserName);
                HttpContext.Session.SetInt32("Subscription_Plan", Subscription_Plan);
                HttpContext.Session.SetString("ServerName", ServerName);

                HttpContext.Session.SetString("Uname", UserName);

            }
            if (HttpContext.Session.GetString("Role") == "TeamMember")
            {
                HttpContext.Session.SetString("Username", Display_Name);
                HttpContext.Session.SetString("Email", Email);
                HttpContext.Session.SetString("Company_Name", Company_Name);
                HttpContext.Session.SetString("Role", Role);
                HttpContext.Session.SetString("Company", ClientID);
                HttpContext.Session.SetString("UserId", TeamMemberID);
                HttpContext.Session.SetInt32("AccType", AccType);
                HttpContext.Session.SetInt32("RoleId", RoleForApp);
                HttpContext.Session.SetString("IsClient", IsClient);
                HttpContext.Session.SetString("Uname", UserName);

                HttpContext.Session.SetString("IsTeamMember", IsTeamMember);
                HttpContext.Session.SetInt32("IsEmailConfirm", IsEmailConfirm);
                HttpContext.Session.SetInt32("IsAuthenticated", IsAuthenticated);
                HttpContext.Session.SetString("Response", Response);
                HttpContext.Session.SetInt32("PowerBIReportAdd", AddPermission);
                HttpContext.Session.SetInt32("PowerBIReportEdit", EditPermission);
                HttpContext.Session.SetString("ServerName", ServerName);
            }
            if (Convert.ToString(HttpContext.Session.GetInt32("RoleId")) == "")
            {
                ViewBag.Message = "You Don't Have Premission To Access Please Contact " + Company_Name + " Admin";
                return View("Index");
            }

            if (HttpContext.Session.GetString("Role") == "Client")
            {
                dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString);
                foreach (var conn in dynamicConns)
                {
                    if (conn.Islive == true)
                    {
                        HttpContext.Session.SetString("ProductionConn", conn.ConnectionString);
                        HttpContext.Session.SetString("DynamicConnection", conn.ConnectionString);
                        HttpContext.Session.SetString("Connection", "Production");
                    }
                    if (conn.Islive == false)
                    {
                        HttpContext.Session.SetString("DevelopmentConn", conn.ConnectionString);
                        //HttpContext.Session.SetString("Connection", "Development");
                    }
                }
            }
            else
            {
                if (HttpContext.Session.GetString("ServerName") == "apitwo.indigoolive.com")
                {
                    dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString, "1");
                    HttpContext.Session.SetString("Connection", "Production");
                }
                else
                {
                    dynamicConns = (List<DynamicConn>)DynamicConnection(ClientID, _UMconnectionString, "0");
                    HttpContext.Session.SetString("Connection", "Development");
                }
                foreach (var con in dynamicConns)
                {
                    //--'apitwo.indigoolive.com'  Is live = 1
                    //--'apione.indigoolive.com'  Is live = 0
                    HttpContext.Session.SetString("DynamicConnection", con.ConnectionString);
                }
            }
            return RedirectToAction("Dashboard", "Home");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult Logout()
        {

        // HttpContext.Response.Headers.Add("Clear-Site-Data", "\"*\"");
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        public IEnumerable<DynamicConn> DynamicConnection(string ClientID, string DbData, string islive = null)
        {
            try
            {
                StringBuilder Sqlquery = new StringBuilder();
                Sqlquery.AppendLine("Select");
                Sqlquery.AppendLine(" Connection_String as ConnectionString ");
                Sqlquery.AppendLine(" ,IS_Live as Islive");
                Sqlquery.AppendLine(" from [masters].[ClientConfig]");
                Sqlquery.AppendLine(" where client_id =  '" + ClientID + "'");
                if (islive != null)
                {
                    Sqlquery.AppendLine(" and IS_Live ='" + islive + "'");

                }
                using (IDbConnection db = new SqlConnection(DbData))
                {
                    return db.Query<DynamicConn>
                    (Sqlquery.ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IActionResult UpdateConnection([FromBody] AjaxShuffled o)
        {
            var dynamic = "";
            HttpContext.Session.Remove("DynamicConnection");
            if (o.shuffled == true)
            {
                dynamic = HttpContext.Session.GetString("ProductionConn");
                HttpContext.Session.SetString("DynamicConnection", dynamic);
                HttpContext.Session.SetString("Connection", "Production");
            }
            else
            {
                dynamic = HttpContext.Session.GetString("DevelopmentConn");
                HttpContext.Session.SetString("DynamicConnection", dynamic);
                HttpContext.Session.SetString("Connection", "Development");
            }
            return new JsonResult("Connection Changes");

        }
        public class AjaxShuffled
        {
            public bool shuffled { get; set; }
        }

    }
}
